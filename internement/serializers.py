from marshmallow import (
    validates_schema,
    fields,
    validate,
    validates,
    ValidationError,
    Schema,
)
from marshmallow.decorators import pre_dump
from utils.serializers import BaseSchema
from workforce.serializers import MedicSchema, PatientSchema
from workforce.models import Medic, Patient
from system.models import System
from internement.models import Internement, InternementSystem


# Create your serializers here.


class InternementSchema(BaseSchema):

    patient_id = fields.Int(required=True, allow_none=False, load_only=True)

    patient = fields.Nested(
        lambda: PatientSchema(exclude=["created", "modified"]),
        required=True,
        allow_none=False,
        dump_only=True,
    )

    medics_ids = fields.List(
        fields.Int(), required=True, allow_none=False, load_only=True
    )

    medic = fields.List(
        fields.Nested(lambda: MedicSchema(exclude=["created", "modified"])),
        dump_only=True,
    )

    current_disease = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )

    date_first_simptoms = fields.Date(
        required=True, allow_none=False, format="%Y-%m-%d"
    )

    date_diagnosis = fields.Date(required=True, allow_none=False, format="%Y-%m-%d")

    check_in = fields.Date(required=True, allow_none=False, format="%Y-%m-%d")

    check_out = fields.Date(required=True, allow_none=True, format="%Y-%m-%d")

    date_of_death = fields.Date(required=True, allow_none=True, format="%Y-%m-%d")

    close_observation = fields.Str(required=False, allow_none=True)

    @validates("medics_ids")
    def validate_medics_ids(self, value):
        for m in value:
            medic = Medic.query.filter_by(id=m, is_active=True).first()
            if not medic:
                raise ValidationError(
                    f"The medic_id: {m} does not exist or is inactive."
                )

    @validates("patient_id")
    def validate_patient_id(self, value):
        patient = Patient.query.filter_by(id=value, is_active=True).first()
        if not patient:
            raise ValidationError(f"The patient_id does not exist or is inactive.")

    @validates_schema
    def validate_dates(self, data, **kwargs):
        internements = Internement.query.filter_by(patient_id=data["patient_id"]).all()
        for internement in internements:
            if not internement.check_out and not internement.date_of_death:
                raise ValidationError(f"There is an already opened internement")
        c_in = data["check_in"]
        c_out = data["check_out"]
        death = data["date_of_death"]
        diagnosis = data["date_diagnosis"]
        simptoms = data["date_first_simptoms"]
        if c_out and c_in > c_out:
            raise ValidationError(f"Check out must be grater or equal to check in")
        if diagnosis > c_in or simptoms > c_in:
            raise ValidationError(
                f"Date of diagnosis/first simptoms must be grater or equal to check in"
            )
        if death and death < c_in:
            raise ValidationError(f"Date of death must be smaller or equal to check in")


class InternementSystemSchema(Schema):
    id = fields.Int()
    date = fields.DateTime(format="%Y-%m-%d %H:%M:%S", allow_none=True)
    internement_id = fields.Int(required=True, allow_none=False)
    system_id = fields.Int(required=True, allow_none=False)
    system = fields.Str(dump_only=True)
    medic_id = fields.Int(required=True, allow_none=False)

    @validates("internement_id")
    def validate_internement_id(self, value):
        internement = Internement.query.filter_by(id=value, is_active=True).first()
        if not internement:
            raise ValidationError(f"The internement_id does not exist or is inactive.")
        if internement.check_out or internement.date_of_death:
            raise ValidationError(f"The internement_id is closed.")

    @validates("system_id")
    def validate_system_id(self, value):
        system = System.query.filter_by(id=value, is_active=True).first()
        if not system:
            raise ValidationError(f"The system_id does not exist or is inactive.")

    @validates("medic_id")
    def validate_medic_id(self, value):
        medic = Medic.query.filter_by(id=value, is_active=True).first()
        if not medic:
            raise ValidationError(f"The medic_id does not exist or is inactive.")

    @validates_schema
    def validate_system(self, data, **kwargs):
        int_sys = (
            InternementSystem.query.filter_by(internement_id=data["internement_id"])
            .order_by(InternementSystem.id.desc())
            .first()
        )
        if int_sys and int_sys.system_id == data["system_id"]:
            raise ValidationError(f"The system 'from' is the same as the system 'to'.")

    """@pre_dump
    def add_system_name(self, data, many, **kwargs):
    
        system_name = System.query.filter_by(id=data.system_id).first().name
        self.system = system_name
        return data"""