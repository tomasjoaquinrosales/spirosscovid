from flask.globals import session
from system.serializers import BedSchema
from system.models import System, Room
from sqlalchemy.exc import DatabaseError, IntegrityError
from marshmallow import ValidationError
from utils.errors import ConflictError, NotFoundError, ClientException
from utils.responses import (
    AbstractValidation,
    GenericCreateResponse,
    GenericListResponse,
    GenericRetrieveResponse,
    GenericDeleteResponse,
)
from sqlalchemy.sql import and_, or_
from utils.serializers import EntityIDSchema, YesOrNotResponseSchema
from internement.models import Internement, InternementSystem
from internement.serializers import InternementSchema, InternementSystemSchema
from workforce.models import Patient, Medic
from workforce.serializers import MedicSchema
from settings.database import Session
from http import HTTPStatus
from flask_jwt_extended import get_jwt_identity
from datetime import datetime

# Create your responses here.


class InternementResponse(GenericCreateResponse, GenericRetrieveResponse):
    model = Internement
    schema = InternementSchema
    session = Session

    def pre_create(self, *args, **kwargs):
        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)
        if system.name != "Guardia":
            raise ClientException(user_err_msg="User not in Guardia")
        try:
            location = dict()
            room = dict()
            bed = dict()
            location["room"] = room
            location["bed"] = bed
            location["room"]["room_id"] = self.data.pop("room_id", None)
            location["bed"]["id"] = self.data.pop("bed_id", None)
            location["bed"]["label"] = self.data.pop("bed_label", None)
            if location["bed"]["id"]:
                BedSchema().load(location["bed"])  # solo para validar
            self.location = location
        except ValidationError as e:
            raise NotFoundError(user_err_msg=e.messages)
        super().pre_create(*args, **kwargs)

    def create_instance(self, *args, **kwargs):
        medic_ids = self.data.pop("medics_ids")
        internement = super().create_instance()

        for medic_id in medic_ids:
            medic = Medic.query.filter_by(id=medic_id).first()
            internement.medic.append(medic)

        """patient = Patient.query.filter_by(id=internement.patient_id).first()
        patient.bed_id = self.s_bed["id"]

        self.session.add(patient)"""
        self.session.add(internement)
        self.session.commit()

        return internement

    def post_create(self, instance):
        int_deserialized = super().post_create(instance)
        chg_deserialized, _ = InternementSystemResponse().create_from_internement(
            int_deserialized, self.location
        )
        act_internement = self.model.query.filter_by(id=int_deserialized["id"]).first()
        response = dict()
        response["internement"] = self.schema().dump(act_internement)
        response["change_system"] = chg_deserialized
        return response


class InternementSystemResponse(GenericCreateResponse):
    model = InternementSystem
    schema = InternementSystemSchema
    session = Session
    location = None

    def __init__(self, request=None):
        if request:
            self.data = request.get_json()

    def create_from_internement(self, internement, location: dict):
        self.location = location
        change_system = dict()
        change_system["date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        change_system["internement_id"] = internement["id"]
        username = get_jwt_identity()["username"]
        medic, system = Medic.get_by_username(username)
        if system.name != "Guardia":
            raise ClientException(user_err_msg="User not in Guardia")
        change_system["system_id"] = system.id
        change_system["medic_id"] = medic.id
        self.data = change_system
        return super().create()

    def pre_create(self, *args, **kwargs):
        try:
            if not self.location:
                location = dict()
                room = dict()
                bed = dict()
                location["room"] = room
                location["bed"] = bed
                location["room"]["room_id"] = self.data.pop("room_id", None)
                location["bed"]["id"] = self.data.pop("bed_id", None)
                location["bed"]["label"] = self.data.pop("bed_label", None)
                if location["bed"]["id"]:
                    BedSchema().load(location["bed"])  # solo para validar
                self.location = location
        except ValidationError as e:
            raise NotFoundError(user_err_msg=e.messages)
        if not self.data["medic_id"]:
            username = get_jwt_identity()["username"]
            medic, _ = Medic.get_by_username(username)
            self.data["medic_id"] = medic.id
        super().pre_create(*args, **kwargs)

    def create_instance(self, *args, **kwargs):
        int_sys = super().create_instance()
        internement = Internement.query.filter_by(id=int_sys.internement_id).first()
        patient = Patient.query.filter_by(id=internement.patient_id).first()

        last_change = (
            self.session.query(InternementSystem)
            .filter(InternementSystem.internement_id == internement.id)
            .order_by(InternementSystem.id.desc())
            .first()
        )

        if last_change:
            if last_change.system_id == 5:  # domicilio
                bed = patient.bed
                patient.bed = None
                self.session.add(patient)
                self.session.commit()
                self.session.delete(bed)
                self.session.commit()
            elif int_sys.system_id < 4:
                medic = (
                    self.session.query(Medic)
                    .join(System)
                    .filter(System.chief_id == Medic.id)
                    .filter(System.id == int_sys.system_id)
                    .first()
                )
                internement.medic = [medic]

                self.session.add(internement)
                self.session.commit()

        if self.location["bed"]["id"]:
            patient.bed_id = self.location["bed"]["id"]
            self.session.add(patient)
            self.session.commit()
        else:
            room = Room.query.filter_by(
                id=self.location["room"]["room_id"], is_room_infinite=True
            ).first()
            if room and self.location["bed"]["label"]:
                from system.models import Bed

                new_bed = Bed(label=self.location["bed"]["label"], room_id=room.id)
                new_bed.patient = patient
                self.session.add(new_bed)
                self.session.commit()
            else:
                raise ClientException(user_err_msg="Not a valid room or bed_label")

        self.session.add(int_sys)
        self.session.commit()

        return int_sys


class InternementListResponse(GenericListResponse):
    model = Internement
    schema = InternementSchema
    session = Session

    def lists(self, *args, **kwargs):
        page_size = kwargs["page_size"]
        page = kwargs["page"]

        page_from = (page - 1) * page_size

        patient = list()
        if "patient" in kwargs:
            patient.append(Patient.id == kwargs["patient"])

        internements = (
            self.session.query(Internement)
            .join(Patient)
            .filter(Internement.created.between(kwargs["from"], kwargs["to"]))
            .filter(or_(*patient))
            .order_by(Internement.id.asc())
            .limit(page_size)
            .offset(page_from)
            .all()
        )

        total_results = Internement.query.filter(
            Internement.created.between(kwargs["from"], kwargs["to"])
        ).count()

        response = dict()
        list_serializer = list()

        if internements:
            for obj in internements:
                serializer = self.schema().dump(obj)
                list_serializer.append(serializer)

        response["status"] = HTTPStatus.OK
        response["totalResults"] = total_results
        response["results"] = list_serializer
        return response, HTTPStatus.OK


class InternementPatientResponse(GenericRetrieveResponse):
    model = Internement
    schema = InternementSchema
    session = Session

    def get_object(self, id: int):
        instance = (
            self.model.query.filter(Internement.patient_id == id)
            .order_by(Internement.id.desc())
            .first()
        )
        if not instance:
            raise ConflictError(
                user_err_msg=f"{self.model.__name__}({id}) does not exists."
            )
        return instance


class InternementMedicResponse(GenericCreateResponse):
    model = Internement
    schema = InternementSchema
    session = Session

    def create(self, patient_id):
        if not self.data:
            raise ClientException(user_err_msg="No input data provided")

        if not self.data["medics"] and not isinstance(self.data["medics"], list):
            raise ClientException(user_err_msg="No input data provided")

        instance = (
            self.session.query(Internement)
            .join(Patient)
            .filter(Patient.id == patient_id)
            .order_by(Internement.id.desc())
            .limit(1)
            .first()
        )
        if not instance:
            raise ConflictError(
                user_err_msg=f"{self.model.__name__}({id}) does not exists."
            )

        medics = list()
        for m in self.data["medics"]:
            medic = Medic.query.filter(Medic.id == m["id"]).first()
            if not medic:
                continue
            medics.append(medic)
        instance.medic = medics

        self.session.add(instance)
        self.session.commit()

        return self.schema().dumps(instance), HTTPStatus.CREATED


class InternementPatientValidationsResponse(AbstractValidation):
    model = Internement
    schema = EntityIDSchema
    session = Session

    def __init__(self, request):
        self.data = request.get_json()

    def pre_validation(self, *args, **kwargs):
        try:
            if not self.data:
                raise ClientException(user_err_msg="No input data provided")

            self.serializer = self.schema().load(self.data)
        except ValidationError as e:
            raise NotFoundError(user_err_msg=e.messages)
        except Exception as e:
            raise e

    def validate(self, *args, **kwargs):
        self.pre_validation(*args, **kwargs)
        internement = (
            self.model.query.filter(Internement.patient_id == self.serializer["id"])
            .filter(
                and_(
                    Internement.check_out.is_(None), Internement.date_of_death.is_(None)
                )
            )
            .first()
        )
        if not internement:
            return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK
        return YesOrNotResponseSchema().dump({"valid": True}), HTTPStatus.OK


class InternementCloseResponse:
    session = Session

    def close(self, request, patient_id):
        if not request.data:
            raise ClientException(user_err_msg="No input data provided")

        close_observation = request.get_json().get("death_observation", None)
        check_out = request.get_json().get("check_out", None)
        death = request.get_json().get("date_of_death", None)

        if not check_out and not death:
            raise ClientException(user_err_msg="Bad dates provided")

        patient = Patient.query.filter_by(id=patient_id).first()
        if not patient:
            raise ClientException(user_err_msg="Bad patient_id provided")

        internement = Internement.query.filter_by(
            patient_id=patient.id, check_out=None, date_of_death=None
        ).first()

        if not internement:
            raise ClientException(user_err_msg="There is no active internements")

        last_change = (
            self.session.query(InternementSystem)
            .filter(InternementSystem.internement_id == internement.id)
            .order_by(InternementSystem.id.desc())
            .first()
        )

        if death:
            internement.date_of_death = death
            if last_change.system_id == 5:  # domicilio
                bed = patient.bed
                patient.bed = None
                self.session.add(patient)
                self.session.commit()
                self.session.delete(bed)
                self.session.commit()
        else:
            internement.check_out = check_out
            patient.bed = None
            self.session.add(patient)
            self.session.commit()

        internement.close_observation = close_observation

        self.session.add(internement)
        self.session.commit()

        return InternementSchema().dump(internement), HTTPStatus.OK
