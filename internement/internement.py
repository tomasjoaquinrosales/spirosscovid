from datetime import datetime
from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from authentication.permissions import (
    staff_required,
)
from utils.decorators import ErrorHandler
from internement.responses import (
    InternementResponse,
    InternementListResponse,
    InternementPatientResponse,
    InternementPatientValidationsResponse,
    InternementSystemResponse,
    InternementCloseResponse,
    InternementMedicResponse,
)
from utils.requests import get_url_params
import logging
from flask_cors import CORS


internement = Blueprint(
    "internement", __name__, template_folder="templates", static_folder="static"
)

CORS(internement)

logger = logging.getLogger("app")


# Create your end-points here.


@internement.route("", methods=["POST"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def add_internement():
    response, status_code = InternementResponse(request).create()
    return response, status_code


@internement.route("<int:internement_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def get_internement(internement_id):
    response, status_code = InternementResponse(request).retrieve(internement_id)
    return response, status_code


@internement.route("list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def internement_list():
    filters = get_url_params(request)
    response, status_code = InternementListResponse(request).lists(**filters)
    return response, status_code


@internement.route("patient/<int:patient_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def get_internement_by_patient_id(patient_id):
    response, status_code = InternementPatientResponse().retrieve(patient_id)
    return response, status_code


@internement.route("change_system/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def change_system():
    response, status_code = InternementSystemResponse(request).create()
    return response, status_code


@internement.route("patient/<int:patient_id>/close/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def close_internement(patient_id):
    response, status_code = InternementCloseResponse().close(request, patient_id)
    return response, status_code


@internement.route("<int:patient_id>/medics/add/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def add_medics_internement(patient_id):
    response, status_code = InternementMedicResponse(request).create(patient_id)
    return response, status_code


# VALIDATIONS


@internement.route("has/patient/<int:patient_id>/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, internement)
@staff_required
def has_internement(patient_id):
    response, status_code = InternementPatientValidationsResponse(request).validate(
        patient_id
    )
    return response, status_code
