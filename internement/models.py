from sqlalchemy import Column, Integer, String, Float, ForeignKey, DateTime, Table
from sqlalchemy.orm import relationship
from settings.database import Base
from utils.models import DateAware
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
import datetime

# Create your model here.


internements_medics = Table(
    "internements_medics",
    Base.metadata,
    Column("internement_id", Integer, ForeignKey("internements.id")),
    Column("medic_id", Integer, ForeignKey("medics.id")),
)


class Internement(DateAware):
    __tablename__ = "internements"

    current_disease = Column(String(150), nullable=False)
    date_first_simptoms = Column(DateTime, nullable=False)
    date_diagnosis = Column(DateTime, nullable=False)
    check_in = Column(DateTime, nullable=False)
    check_out = Column(DateTime, nullable=True)
    date_of_death = Column(DateTime, nullable=True)
    evolution = relationship(
        "Evolution", back_populates="internement", cascade="all, delete"
    )
    patient_id = Column(Integer, ForeignKey("patients.id", ondelete="CASCADE"))
    patient = relationship(
        "Patient", back_populates="internement", cascade="all, delete"
    )
    medic = relationship(
        "Medic",
        secondary=internements_medics,
        back_populates="internements",
        cascade="all, delete",
    )
    close_observation = Column(String(150), nullable=True)

    systems = relationship("System", secondary="internements_systems")

    def __repr__(self):
        return f"<Internement {self.id}>"


class InternementSystem(Base):
    __tablename__ = "internements_systems"

    id = Column(Integer, primary_key=True)
    date = Column(DateTime, default=datetime.datetime.today())
    internement_id = Column(Integer, ForeignKey("internements.id"))
    system_id = Column(Integer, ForeignKey("systems.id"))
    medic_id = Column(Integer, nullable=False)
    internement = relationship("Internement", backref="system_assoc")
    system = relationship("System", backref="internement_assoc")

    def __init__(self, internement_id, system_id, date=None, medic_id=0):
        self.internement_id = internement_id
        self.system_id = system_id
        self.medic_id = medic_id
        self.date = date

    def __repr__(self):
        return f"<InternementSystem id {self.id}, {self.internement}, {self.system}, {self.date}>"
