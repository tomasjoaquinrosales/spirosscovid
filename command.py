from pathlib import Path
import click
import os
from app import create_app, bcrypt
from settings.settings import DevelopmentConfig, ProductionConfig, TestingConfig
from utils.click_styles import Aborted, Created, Dropped, Initialized
from flask_bcrypt import generate_password_hash
from flask import jsonify
import datetime
import json


ROOT = Path(os.path.dirname(os.path.abspath(__file__)))


def callback(ctx, param, value):
    if not value:
        ctx.abort()


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--env",
    "-e",
    type=click.Choice(["development", "production", "testing"], case_sensitive=False),
    default="development",
    help="la help q no se para que es.",
)
def init_system(env):
    """Initializes the entire app with the basics to run"""
    try:
        app = create_app()
        app.app_context().push()
        with app.app_context():
            _drop_db()
            _init_db()
            # _create_su("admin")
            _init_system(env)
    except Exception as e:
        click.echo(Aborted(f"Init-system produced the followings errors: {e}."))


def _init_system(env="development"):
    from settings.database import Session
    from system.models import System, Room, Bed
    from evolution.models import Rule

    rule = Rule(
        drowsiness=True,
        breathing_frequency=30,
        days_dif=10,
        saturation=92,
        saturation_compare=3,
    )
    Session.add(rule)
    Session.commit()

    # GUARDIA ############################################

    guardia = System("Guardia", True)
    Session.add(guardia)
    Session.commit()
    rooms = []
    for r in range(5):
        beds = []
        if r < 4:
            rooms.append(Room(f"Habitacion {r+1}", False))
            for b in range(10):
                beds.append(Bed())
            rooms[r].beds = beds
        else:
            rooms.append(Room("Fuera de sala", True))
    guardia = System.query.filter_by(name="Guardia").first()
    guardia.rooms = rooms
    Session.add(guardia)
    Session.commit()

    # UTI Y COVID ########################################

    uti = System("UTI", False)
    covid = System("Piso COVID", False)
    Session.add(uti)
    Session.add(covid)
    Session.commit()

    for system in ["UTI", "Piso COVID"]:
        rooms = []
        for r in range(5):
            beds = []
            rooms.append(Room(f"Habitacion {r+1}", False))
            for b in range(10):
                beds.append(Bed())
            rooms[r].beds = beds
        sys = System.query.filter_by(name=system).first()
        sys.rooms = rooms
        Session.add(sys)
        Session.commit()

    # HOTEL ########################################

    hotel = System("Hotel")
    Session.add(hotel)
    Session.commit()
    rooms = []
    for r in range(5):
        beds = []
        rooms.append(Room(f"Piso {r+1}", False))
        for b in range(10):
            beds.append(Bed())
        rooms[r].beds = beds
    hotel = System.query.filter_by(name="Hotel").first()
    hotel.rooms = rooms
    Session.add(hotel)
    Session.commit()

    # DOMICILIO ########################################

    domicilio = System("Domicilio", True)
    Session.add(domicilio)
    Session.commit()
    room = Room("Domicilio", True)
    domicilio = System.query.filter_by(name="Domicilio").first()
    domicilio.rooms.append(room)
    Session.add(domicilio)
    Session.commit()

    if env in ["development", "testing"]:
        from workforce.models import User, Medic, Patient

        usuarios = []
        usuarios.append(
            User(
                username="med1",
                password=bcrypt.generate_password_hash("med1", 10).decode("utf-8"),
                is_medic=True,
            )
        )
        usuarios.append(
            User(
                username="chief_med1",
                password=bcrypt.generate_password_hash("chief_med1", 10).decode(
                    "utf-8"
                ),
                is_medic=True,
            )
        )
        usuarios.append(
            User(
                username="med2",
                password=bcrypt.generate_password_hash("med2", 10).decode("utf-8"),
                is_medic=True,
            )
        )
        usuarios.append(
            User(
                username="chief_med2",
                password=bcrypt.generate_password_hash("chief_med2", 10).decode(
                    "utf-8"
                ),
                is_medic=True,
            )
        )
        usuarios.append(
            User(
                username="med3",
                password=bcrypt.generate_password_hash("med3", 10).decode("utf-8"),
                is_medic=True,
            )
        )
        usuarios.append(
            User(
                username="chief_med3",
                password=bcrypt.generate_password_hash("chief_med3", 10).decode(
                    "utf-8"
                ),
                is_medic=True,
            )
        )
        Session.bulk_save_objects(usuarios)
        Session.commit()

        guardia = System.query.filter_by(name="Guardia").first()
        uti = System.query.filter_by(name="UTI").first()
        covid = System.query.filter_by(name="Piso COVID").first()

        med1 = User.query.filter_by(username="med1").first()
        chief_med1 = User.query.filter_by(username="chief_med1").first()
        med2 = User.query.filter_by(username="med2").first()
        chief_med2 = User.query.filter_by(username="chief_med2").first()
        med3 = User.query.filter_by(username="med3").first()
        chief_med3 = User.query.filter_by(username="chief_med3").first()
        medicos = []
        medicos.append(Medic(user_id=med1.id, docket="123", system_id=guardia.id))
        medicos.append(
            Medic(
                user_id=chief_med1.id,
                docket="124",
                is_chief=True,
                system_id=guardia.id,
            )
        )
        medicos.append(Medic(user_id=med2.id, docket="125", system_id=uti.id))
        medicos.append(
            Medic(
                user_id=chief_med2.id,
                docket="126",
                is_chief=True,
                system_id=uti.id,
            )
        )
        medicos.append(Medic(user_id=med3.id, docket="127", system_id=covid.id))
        medicos.append(
            Medic(
                user_id=chief_med3.id,
                docket="128",
                is_chief=True,
                system_id=covid.id,
            )
        )
        Session.bulk_save_objects(medicos)
        Session.commit()

        chief_med1 = (
            Medic.query.join(User.medic).filter(User.username == "chief_med1").first()
        )
        guardia.chief_id = chief_med1.id
        chief_med2 = (
            Medic.query.join(User.medic).filter(User.username == "chief_med2").first()
        )
        uti.chief_id = chief_med2.id
        chief_med3 = (
            Medic.query.join(User.medic).filter(User.username == "chief_med3").first()
        )
        covid.chief_id = chief_med3.id
        Session.add(guardia)
        Session.add(uti)
        Session.add(covid)
        Session.commit()

        import datetime

        pacientes = []
        pacientes.append(
            Patient(
                medical_coverage="OS 1",
                dni="33345678",
                first_name="Juan Pablo",
                last_name="Garcia",
                address="Calle 47, 628",
                telephone="0221-4875757",
                date_of_birth=datetime.datetime(1988, 4, 30),
                other_information="other...",
            )
        )

        pacientes.append(
            Patient(
                medical_coverage="OS 1",
                dni="12658978",
                first_name="Graciela",
                last_name="Perez",
                address="Calle 7, 88",
                telephone="0223-4876557",
                date_of_birth=datetime.datetime(1955, 7, 10),
                other_information="other...",
            )
        )

        pacientes.append(
            Patient(
                medical_coverage="OS 2",
                dni="30348528",
                first_name="Tomas",
                last_name="Rosales",
                address="Calle 1, s/n",
                telephone="0221-4111111",
                date_of_birth=datetime.datetime(1990, 1, 1),
                other_information="other...",
            )
        )

        Session.bulk_save_objects(pacientes)
        Session.commit()

        from internement.models import Internement, InternementSystem

        p1 = Patient.query.filter_by(dni="33345678").first()
        int1 = Internement(
            current_disease="Tos, falta de olfato",
            date_diagnosis=datetime.datetime(2020, 10, 9),
            date_first_simptoms=datetime.datetime(2020, 10, 7),
            check_in=datetime.datetime(2020, 10, 9),
            patient_id=p1.id,
            check_out=datetime.datetime(2020, 10, 25),
            date_of_death=None,
        )
        med1 = Medic.query.filter_by(docket="124").first()
        int1.medic.append(med1)
        Session.add(int1)
        Session.commit()
        int1 = Internement.query.filter_by(patient_id=p1.id).first()
        guardia = System.query.filter_by(name="Guardia").first()
        uti = System.query.filter_by(name="UTI").first()
        ingreso = InternementSystem(
            internement_id=int1.id,
            medic_id=med1.id,
            system_id=guardia.id,
            date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
        trasnf = InternementSystem(
            internement_id=int1.id,
            medic_id=med1.id,
            system_id=uti.id,
            date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
        Session.add(ingreso)
        Session.add(trasnf)
        Session.commit()
    click.echo(Initialized(f"System initialized."))


@cli.command()
@click.option(
    "-n",
    "--name",
    "name",
    prompt="Superuser Name",
    help="Superuser Name",
    type=str,
    metavar="<text>",
)
def createsuperuser(name):
    """Create a superuser admin"""
    try:
        app = create_app()
        app.app_context().push()
        with app.app_context():
            _create_su(name)
    except Exception as e:
        click.echo(Aborted(f"Create superuser produce the followings errors: {e}."))


def _create_su(name="admin"):
    from settings.database import Session
    from workforce.models import User
    from workforce.serializers import UserSchema

    user = User.query.filter(User.username == name).first()
    if user:
        raise Exception(f"The {user} already exists.")
    else:

        password_crypt = bcrypt.generate_password_hash("admin", 10).decode("utf-8")

        user = User(name, password_crypt, True, False, False)

        Session.add(user)
        Session.commit()

        serializer = UserSchema().dump(user)

        response = dict()
        response["user"] = serializer
        response["password"] = "The password is: `admin`. INSECURE ! CHANGE IT PLEASE."
        click.echo(Created(f"{response}"))


@cli.command()
@click.option(
    "--yes",
    is_flag=True,
    callback=callback,
    expose_value=False,
    prompt="Do you want to continue?",
    help="Continue or abort execution",
)
def init_db():
    """Init database"""
    try:
        app = create_app()
        app.app_context().push()
        with app.app_context():
            _init_db()
    except Exception as e:
        click.echo(Aborted(f"Init database produce the following error: {e}."))


def _init_db():
    from settings.database import init_db

    init_db()
    click.echo(Initialized(f"Database initialized."))


@cli.command()
@click.option(
    "--yes",
    is_flag=True,
    callback=callback,
    expose_value=False,
    prompt="Do you want to continue?",
    help="Continue or abort execution",
)
def drop_db():
    """Drop database"""
    try:
        app = create_app()
        app.app_context().push()
        with app.app_context():
            _drop_db()
    except Exception as e:
        click.echo(Aborted(f"Dropped database produce the following error: {e}."))


def _drop_db():
    from settings.database import drop_db

    drop_db()
    click.echo(Dropped(f"Database dropped."))


@cli.command()
@click.option(
    "-n",
    "--name",
    "name",
    prompt="Application Name",
    help="The application name",
    type=str,
    metavar="<text>",
)
def startapp(name):
    """Create an application

    NAME is the name of the application, must be unique.
    """

    module = name + ".py"

    main_dir_names = ["static", "templates"]
    main_fil_names = [
        {"__init__.py": ""},
        {
            "models.py": "from sqlalchemy import Column, Integer, String, Float, ForeignKey, DateTime, Table\nfrom sqlalchemy.orm import relationship\nfrom settings.database import Base\nfrom utils.models import DateAware\nfrom flask_jwt_extended import (\n    create_access_token,\n    get_jwt_identity,\n    jwt_required\n)\n\n# Create your model here.\n\n"
        },
        {
            "responses.py": "from sqlalchemy.exc import DatabaseError, IntegrityError\nfrom marshmallow import ValidationError\nfrom utils.errors import (\n    ConflictError,\n    NotFoundError,\n    ClientException\n)\nfrom utils.responses import (\n    GenericCreateResponse,\n    GenericListResponse,\n    GenericRetrieveResponse,\n    GenericDeleteResponse\n)\nfrom settings.database import Session\nfrom http import HTTPStatus\n\n# Create your responses here.\n\n"
        },
        {
            "serializers.py": "from marshmallow import Schema, fields, validate\nfrom utils.serializers import BaseSchema\nfrom utils.serializer_validations import must_not_be_blank\n\n\n# Create your serializers here.\n\n"
        },
        {
            module: "from flask import Blueprint, request\nfrom flask_jwt_extended import jwt_required\nfrom authentication.permissions import admin_required, chief_required, staff_required\nfrom utils.decorators import ErrorHandler\nimport logging\nfrom flask_cors import CORS\n\n\n"
            + f"{name} = Blueprint('{name}', __name__, template_folder='templates', static_folder='static')\n\nCORS({name})\n\nlogger = logging.getLogger('app')\n\n\n# Create your end-points here.\n\n"
        },
    ]

    directory = os.path.join(ROOT, name)

    if not os.path.exists(directory):
        for i in range(0, len(main_dir_names)):
            try:
                dirname = directory + "/" + str(main_dir_names[i])
                os.makedirs(dirname)
                if main_dir_names[i] == "static":
                    dirname = dirname + "/images"
                    os.makedirs(dirname)
            except FileExistsError:
                click.echo(Aborted(f"Directory {directory} already exists."))
        for item in main_fil_names:
            for k, v in item.items():
                try:
                    dirname = directory + "/" + str(k)
                    with open(dirname, "w") as out_file:
                        out_file.write(v)
                except FileExistsError:
                    click.echo(Aborted(f"Directory {directory} already exists."))
        click.echo(Created(f"{name} application created."))
    else:
        click.echo(Aborted(f"Directory {directory} already exists."))


@cli.command()
@click.option(
    "-c",
    "--cls",
    "cls",
    prompt="Class Name",
    help="The Class name",
    type=str,
    metavar="<text>",
)
def dumpdata(cls):
    try:
        app = create_app()
        app.app_context().push()
        with app.app_context():
            from sqlalchemy.ext.serializer import loads, dumps
            from sqlalchemy import inspect
            from sqlalchemy import text
            from settings.database import Session, Base, engine
            from workforce.models import User
            from workforce.serializers import UserSchema

            Model = Base.metadata.tables.get(f"{cls}s", None)

            if Model is not None:

                query = Session.query(Model).order_by(text("id asc")).all()

                # serialized = UserSchema().dump(query, many=True)

                serialized = dumps(query)

                query2 = loads(serialized, Base.metadata, Session)

                json_file = f"{cls}s.json"
                with open(json_file, "w") as outfile:
                    json.dump(
                        query2, sort_keys=True, indent=1, default=default, fp=outfile
                    )

                click.echo(Created(f"dumpata({json_file})"))
    except Exception as e:
        click.echo(Aborted(f"Dumdata produce the followings errors: {e}."))


@cli.command()
@click.option(
    "--env",
    type=click.Choice(["development", "production", "testing"], case_sensitive=False),
    default="development",
    help="Start the application.",
)
def runserver(env):
    """Run Application Server"""
    if env in ["development", "testing"]:
        try:
            app = create_app(DevelopmentConfig())
            app.run()
        except Exception as e:
            click.echo(Aborted(f"The following error occur: {e}."))
    elif env in ["production"]:
        try:
            app = create_app(ProductionConfig())
            app.run()
        except Exception as e:
            click.echo(Aborted(f"The following error occur: {e}."))


def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


if __name__ == "__main__":
    cli()
