from app import create_app
import os


env = os.environ.get("FLASK_ENV", "development")
if __name__ == "__main__":
    app = create_app(env)
    app.run()
