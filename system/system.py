from flask import Blueprint, request
from utils.decorators import ErrorHandler
from flask_jwt_extended import get_jwt_claims, jwt_required, get_jwt_identity
from authentication.permissions import admin_required, chief_required
from flask_cors import CORS
from system.responses import (
    SystemResponse,
    SystemListResponse,
    RoomResponse,
    BedResponse,
    DashboardResponse,
    SystemValidation
)
from system.models import System, Room, Bed
from internement.models import Internement, InternementSystem
from workforce.models import Medic, User
import logging
from settings.database import Session


system = Blueprint(
    "system", __name__, template_folder="templates", static_folder="static"
)

CORS(system)

logger = logging.getLogger("app")


###################
# SYSTEM ########## solo superuser
###################

# Testeado
@system.route("", methods=["POST"])
@jwt_required
@ErrorHandler(logger, system)
def create_system():
    response, status_code = SystemResponse(request).create()
    return response, status_code


# Testeado
@system.route("list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def lists_system():
    response, status_code = SystemResponse(request).lists()
    return response, status_code

# Testeado
@system.route("patient/<int:patient_id>/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def lists_system_by_patient_id(patient_id):
    response, status_code = SystemListResponse(request).lists(patient_id)
    return response, status_code


# Testeado
@system.route("<int:system_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def get_system(system_id):
    response, status_code = SystemResponse(request).retrieve(system_id)
    return response, status_code


# Testeado
@system.route("<int:system_id>/", methods=["DELETE"])
@jwt_required
@ErrorHandler(logger, system)
def delete_system(system_id):
    response, status_code = SystemResponse(request).delete(system_id)
    return response, status_code


# Testeado
@system.route("<int:system_id>/", methods=["PATCH"])
@jwt_required
@ErrorHandler(logger, system)
def update_system(system_id):
    response, status_code = SystemResponse(request).partial_update(system_id)
    return response, status_code


@system.route("test/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def test():
    """
    u1 = User('med1','asd')
    u2 = User('med2','sad')
    u3 = User('med3','sad')
    Session.add(u1)
    Session.add(u2)
    Session.add(u3)
    Session.commit()
    u1 = User.query.filter_by(
        username='med1'
    ).first()
    u2 = User.query.filter_by(
        username='med2'
    ).first()
    u3 = User.query.filter_by(
        username='med3'
    ).first()
    m1 = Medic(u1.id,'123')
    m2 = Medic(u2.id,'124')
    m3 = Medic(u3.id,'134')
    Session.add(m1)
    Session.add(m2)
    Session.add(m3)
    Session.commit()
    """
    """
    b1 = Bed()
    b2 = Bed()
    b3 = Bed()
    Session.add(b1)
    Session.add(b2)
    Session.add(b3)
    Session.commit()

    r1 = Room("Habitacion 1")
    r2 = Room("Habitacion 1")
    r1.beds.append(b1)
    r1.beds.append(b2)
    r2.beds.append(b3)
    Session.add(r1)
    Session.add(r2)
    Session.commit()

    m1 = Medic.query.filter_by(
        docket='123'
    ).first()
    m2 = Medic.query.filter_by(
        docket='124'
    ).first()
    m3 = Medic.query.filter_by(
        docket='134'
    ).first()

    guardia = System('Guardia')
    uti = System('UTI')
    uti.rooms.append(r1)
    uti.medics.append(m1)
    guardia.rooms.append(r2)
    guardia.medics.append(m2)
    guardia.medics.append(m3)
    Session.add(guardia)
    Session.add(uti)
    Session.commit()
    """
    """
    uti = System.query.filter_by(name="UTI").first()
    guardia = System.query.filter_by(name="Guardia").first()
    print(f'System UTI {uti.name}')
    print(f'Medicos UTI {uti.medics}')
    print(f'Salas UTI {uti.rooms}')
    print(f'Camas UTI {uti.rooms[0].beds}')
    print(f'System G {guardia.name}')
    print(f'Medicos G {guardia.medics}')
    print(f'Salas G {guardia.rooms}')
    print(f'Camas G {guardia.rooms[0].beds}')
    """
    """
    internacion = Internement.query.filter_by(id=1).first()
    guardia = System.query.filter_by(name="Guardia").first()
    registro_guardia = InternementSystem(internacion.id, guardia.id)
    Session.add(registro_guardia)
    Session.commit()
    """
    # registro_guardia = InternementSystem.query.first()
    # print(registro_guardia)
    """
    internacion = Internement.query.filter_by(id=1).first()
    uti = System.query.filter_by(name="UTI").first()
    transferencia = InternementSystem(internacion.id, uti.id)
    Session.add(transferencia)
    Session.commit()
    """
    # registro_transferencia = InternementSystem.query.filter_by(id=3).first()
    # print(registro_transferencia)
    # """
    #

    username = get_jwt_identity()["username"]
    medic, system = Medic.get_by_username(username)

    # return f"{get_system_from_user(username)}""
    return f"Medico {medic} en sistema {system}"


###################
# ROOM ############
###################

#
@system.route("room/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, system)
def create_room():
    response, status_code = RoomResponse(request).create()
    return response, status_code


#


@system.route("room/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def lists_room():
    response, status_code = RoomResponse(request).lists()
    return response, status_code


#
@system.route("room/<int:system_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def get_room(system_id):
    response, status_code = SystemResponse(request).retrieve(system_id)
    return response, status_code


#
@system.route("available_room/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def available_room():
    system_id = request.args.get(
        "system_id",
        default=None,
        type=int,
    )
    if not system_id:
        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)
        system_id = system.id
    response, status_code = RoomResponse(request).lists_available(request, system_id)
    return response, status_code


@system.route("room/<int:system_id>/", methods=["DELETE"])
@jwt_required
@ErrorHandler(logger, system)
def delete_room(system_id):
    response, status_code = SystemResponse(request).delete(system_id)
    return response, status_code


@system.route("room/<int:system_id>/", methods=["PATCH"])
@jwt_required
@ErrorHandler(logger, system)
def update_room(system_id):
    response, status_code = SystemResponse(request).partial_update(system_id)
    return response, status_code


###################
# BED #############
###################

# Tested
@system.route("bed/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, system)
def create_bed():
    response, status_code = BedResponse(request).create()
    return response, status_code


# Tested
@system.route("bed/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def lists_bed():
    response, status_code = BedResponse(request).lists()
    return response, status_code


#
@system.route("available_bed/<int:room_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def available_bed(room_id):
    response, status_code = BedResponse(request).lists_available(room_id)
    return response, status_code


# Tested
@system.route("bed/<int:bed_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
def get_bed(bed_id):
    response, status_code = BedResponse(request).retrieve(bed_id)
    return response, status_code


# Tested
@system.route("bed/<int:bed_id>/", methods=["DELETE"])
@jwt_required
@ErrorHandler(logger, system)
def delete_bed(bed_id):
    response, status_code = BedResponse(request).delete(bed_id)
    return response, status_code


# Tested
@system.route("bed/<int:bed_id>/", methods=["PATCH"])
@jwt_required
@ErrorHandler(logger, system)
def update_bed(bed_id):
    response, status_code = BedResponse(request).partial_update(bed_id)
    return response, status_code


#########################
# DASHBOARD #############
#########################


@system.route("dashboard/summary/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
@chief_required
def dashboard_summary():
    response, status_code = DashboardResponse(request).get()
    return response, status_code


@system.route("<int:system_id>/validate/beds/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, system)
@chief_required
def validate_system_destination(system_id):
    response, status_code = SystemValidation().validate_beds(system_id)
    return response, status_code
