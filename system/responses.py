from sqlalchemy.sql.elements import and_

from internement.models import InternementSystem, Internement
from utils.serializers import ResponseSchema, YesOrNotResponseSchema
from workforce.models import Medic, Patient
from system.models import System, Room, Bed
from system.serializers import SystemSchema, RoomSchema, BedSchema
from utils.responses import (
    AbstractResponse,
    GenericCreateResponse,
    GenericListResponse,
    GenericRetrieveResponse,
    GenericDeleteResponse,
    GenericUpdateResponse,
)
from app import bcrypt
from http import HTTPStatus
from settings.database import Session
from flask_jwt_extended import get_jwt_identity


# Create your responses here.


class SystemResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
    GenericUpdateResponse,
):
    model = System
    schema = SystemSchema
    session = Session

    def create_instance(self, *args, **kwargs):
        system = System(
            name=self.serializer["name"],
            has_infinite_beds=self.serializer["has_infinite_beds"],
            chief_id=self.serializer["chief_id"],
        )
        rooms_ids = self.serializer["rooms_ids"]
        for room_id in rooms_ids:
            room = Room.query.filter_by(id=room_id).first()
            system.rooms.append(room)
        return system


class SystemListResponse(GenericListResponse):
    model = System
    schema = SystemSchema
    session = Session

    def lists(self, patient_id):
        system = (
            self.session.query(System)
            .join(InternementSystem)
            .join(Internement)
            .join(Patient)
            .filter(Patient.id == patient_id)
            .filter(
                and_(
                    Internement.date_of_death.is_(None), Internement.check_out.is_(None)
                )
            )
            .order_by(InternementSystem.id.desc())
            .first()
        )
        if system.name.upper() == "GUARDIA":
            self.queryset = (
                self.session.query(System)
                .filter(System.name.in_(("UTI", "Piso COVID")))
                .all()
            )
        elif system.name.upper() == "UTI":
            self.queryset = (
                self.session.query(System).filter(System.name == "Piso COVID").all()
            )
        elif system.name.upper() == "PISO COVID":
            self.queryset = (
                self.session.query(System)
                .filter(System.name.in_(("Hotel", "Domicilio", "UTI")))
                .all()
            )
        elif system.name.upper() == "HOTEL" or system.name.upper() == "DOMICILIO":
            self.queryset = (
                self.session.query(System).filter(System.name == "Piso COVID").all()
            )

        return super(SystemListResponse, self).lists()


class BedResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
    GenericUpdateResponse,
):
    model = Bed
    schema = BedSchema
    session = Session

    def lists_available(self, room_id):
        beds = self.model.query.filter_by(room_id=room_id, is_active=True).all()
        available_beds = []
        for bed in beds:
            if bed.patient is None:
                available_beds.append(bed)
        response = dict()
        response["response"] = self.schema().dump(obj=available_beds, many=True)
        return response, HTTPStatus.OK


class RoomResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
    GenericUpdateResponse,
):
    model = Room
    schema = RoomSchema
    session = Session

    def create_instance(self, *args, **kwargs):
        room = Room(
            name=self.serializer["name"],
            is_room_infinite=self.serializer["is_room_infinite"],
        )
        beds_ids = self.serializer["beds_ids"]
        for bed_id in beds_ids:
            bed = Bed.query.filter_by(id=bed_id).first()
            room.beds.append(bed)
        return room

    def lists_available(self, request, system_id):
        all_rooms = self.model.query.filter_by(
            system_id=system_id, is_active=True
        ).all()
        available_rooms = []
        for room in all_rooms:
            beds, _ = BedResponse(request).lists_available(room.id)
            if beds["response"] or room.is_room_infinite:
                available_rooms.append(room)
        response = dict()
        response["response"] = self.schema().dump(obj=available_rooms, many=True)
        return response, HTTPStatus.OK


class DashboardResponse:
    session = Session

    def __init__(self, request):
        self.request = request

    def get(self):
        from settings.database import engine
        from workforce.serializers import PatientSchema
        from flask import jsonify

        system_id = self.request.args.get(
            "systemId",
            default=None,
            type=int,
        )

        if not system_id:
            username = get_jwt_identity()["username"]
            _, system = Medic.get_by_username(username)
        else:
            system = System.query.filter(System.id == system_id).first()

        if not system:
            return jsonify({"result": []}), HTTPStatus.OK

        query_occupation_by_room = (
            "select r2.id, COUNT(r2.id), COUNT(p2.id)"
            + "from systems s"
            + " "
            + "inner join rooms r2 on r2.system_id  = s.id"
            + " "
            + "inner join beds b2 on b2.room_id = r2.id"
            + " "
            + "left join patients p2 on p2.bed_id = b2.id"
            + " "
            + f"where s.id = {system.id}"
            + " "
            + "group by r2.id"
            + " "
            + "order by r2.id asc;"
        )

        resultset = engine.execute(query_occupation_by_room)

        response = dict()
        response["rooms"] = rooms = list()

        for rs in resultset:
            room_object = Room.query.filter_by(id=rs[0]).first()
            room = dict()
            rooms.append(room)
            room["id"] = rs[0]
            room["name"] = room_object.name
            room["beds"] = rs[1]
            room["occupation"] = rs[2]
            room["patients"] = patients = list()
            query_patients_by_room = (
                "select p2.*"
                + " "
                + "from rooms r2"
                + " "
                + "inner join beds b2 on b2.room_id = r2.id"
                + " "
                + "left join patients p2 on p2.bed_id = b2.id"
                + " "
                + f"where r2.id = {rs[0]} and p2 is not null;"
            )
            resultset_patients = engine.execute(query_patients_by_room)
            for p in resultset_patients:
                patients.append(PatientSchema().dump(p))
        return jsonify({"result": response}), HTTPStatus.OK


class SystemValidation:
    session = Session

    def validate_beds(self, to_system_id):

        system = System.query.filter_by(id=to_system_id).first()
        rooms = Room.query.filter_by(system_id=system.id).all()
        count = 0
        for room in rooms:
            for bed in room.beds:
                if not bed.patient:
                    count = count + 1
        if count > 0 or system.name.lower() == 'domicilio':
            return YesOrNotResponseSchema().dump({"valid": True}), HTTPStatus.OK
        return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK
