from workforce.serializers import MedicSchema
from workforce.models import Patient
from marshmallow import (
    Schema,
    fields,
    validate,
    validates,
    ValidationError,
    validates_schema,
)
from marshmallow.decorators import pre_load
from utils.serializers import BaseSchema
from system.models import System, Room, Bed
from settings.database import Session


# Create your serializers here.


class SystemSchema(BaseSchema):
    name = fields.Str(required=True, allow_none=False, validate=validate.Length(min=3))
    has_infinite_beds = fields.Boolean(required=True, allow_none=True)
    chief_id = fields.Int(required=True, allow_none=True)

    medics = fields.List(
        fields.Nested(lambda: MedicSchema(exclude=["created", "modified"])),
        dump_only=True,
    )

    rooms_ids = fields.List(
        fields.Integer, load_only=True, required=True, allow_none=False
    )

    rooms = fields.List(
        fields.Nested(lambda: RoomSchema(exclude=["created", "modified"])),
        dump_only=True,
    )

    @validates("name")
    def validate_name(self, value):
        if not value:
            raise ValidationError(f"The name field is required")
        system = System.query.filter_by(name=value, is_active=True).first()
        if system:
            raise ValidationError(f"The name: {value} already exist.")

    @validates("rooms_ids")
    def validate_rooms_ids(self, value):
        if not value:
            raise ValidationError(f"The name field is required")
        used_rooms = (
            Session.query(Room).join(System).filter(Room.system_id == System.id).all()
        )
        for room_id in value:
            room = Room.query.filter_by(id=room_id).first()
            if not room:
                raise ValidationError(f"The id: {room_id} does not exist.")
            if room in used_rooms:
                raise ValidationError(f"The id: {room_id} is used in another system.")


class RoomSchema(BaseSchema):
    name = fields.Str(required=True, allow_none=False, validate=validate.Length(min=3))
    is_room_infinite = fields.Boolean(required=True, allow_none=True)

    beds_ids = fields.List(
        fields.Integer, load_only=True, required=True, allow_none=False
    )

    """beds = fields.List(fields.Nested(lambda: BedSchema(
        exclude=["created", "modified"])), dump_only=True)"""

    @validates("name")
    def validate_name(self, value):
        if not value:
            raise ValidationError(f"The name field is required")
        room = Room.query.filter_by(name=value, is_active=True).first()
        if room:
            raise ValidationError(f"The name: {value} already exist.")

    @validates("beds_ids")
    def validate_beds_ids(self, value):
        if not value:
            raise ValidationError(f"The name field is required")
        used_bed = Session.query(Bed).join(Room).filter(Bed.room_id == Room.id).all()
        for bed_id in value:
            bed = Bed.query.filter_by(id=bed_id).first()
            if not bed:
                raise ValidationError(f"The id: {bed_id} does not exist.")
            if bed in used_bed:
                raise ValidationError(f"The id: {bed_id} is used in another room.")


class BedSchema(Schema):
    id = fields.Int(required=True, allow_none=True)
    label = fields.Str(required=True, allow_none=True)

    @pre_load(pass_many=True)
    def validate_ocupancy(self, data, many, **kwargs):
        if not many:
            patient = Patient.query.filter_by(bed_id=data["id"]).first()
            if patient:
                raise ValidationError(f"The bed is already taken.")
        return data
