from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    ForeignKey,
    DateTime,
    Table,
    Boolean,
    Enum,
)
from sqlalchemy.orm import relationship
from utils.models import DateAware
from internement.models import Internement


class System(DateAware):
    __tablename__ = "systems"

    name = Column(String(100), unique=True, nullable=False)
    has_infinite_beds = Column(Boolean, default=False)
    chief_id = Column(Integer, nullable=True)

    medics = relationship("Medic", back_populates="system", cascade="all, delete")

    rooms = relationship("Room", back_populates="system", cascade="all, delete")

    internements = relationship("Internement", secondary="internements_systems")

    def __init__(self, name, has_infinite_beds=False, chief_id=None):
        self.name = name
        self.has_infinite_beds = has_infinite_beds
        self.chief_id = chief_id

    def __repr__(self):
        return f"{self.name}"


class Room(DateAware):
    __tablename__ = "rooms"

    name = Column(
        String(100), unique=False, nullable=False
    )  # TODO validar que el nombre sea unico para un sistema
    is_room_infinite = Column(Boolean, default=False)
    system_id = Column(Integer, ForeignKey("systems.id", ondelete="CASCADE"))
    system = relationship("System", back_populates="rooms", cascade="all, delete")

    beds = relationship("Bed", back_populates="room", cascade="all, delete")

    def __init__(self, name, is_room_infinite=False):
        self.name = name
        self.is_room_infinite = is_room_infinite

    def __repr__(self):
        return f"<Room {self.name}>"


class Bed(DateAware):
    __tablename__ = "beds"

    label = Column(String(100))

    room_id = Column(Integer, ForeignKey("rooms.id", ondelete="CASCADE"))
    room = relationship("Room", back_populates="beds")

    patient = relationship(
        "Patient", uselist=False, back_populates="bed", cascade="all, delete"
    )

    def __repr__(self):
        return f"<Bed {self.id}>"
