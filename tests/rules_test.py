import pytest
import json
from datetime import datetime


def get_token(client):
    data = {
        "username": "chief_med2",
        "password": "chief_med2"
    }
    response = client.post("/auth/login/", data=json.dumps(data), headers={"Content-Type": "application/json"})
    access_token = response.json['access_token']
    return access_token


def create_evolution(
        drowsiness=False,
        medic_id=1,
        internement_id=2,
        temperature=12,
        blood_pressures_s=12,
        blood_pressure_d=14,
        cardiac_frequency=15,
        breathing_frequency=25,
        ventilator='todo good',
        o2=None,
        o2_saturation=93,
        pafi=18,
        cough=True,
        observation=None,
        evolution_date=datetime.now().strftime("%Y-%m-%d %H:%M:%S")
):
    evolution = {
        "medic_id": medic_id,
        "internement_id": internement_id,
        "evolution_template": {
            "drowsiness": drowsiness,
            "temperature": temperature,
            "blood_pressures_s": blood_pressures_s,
            "blood_pressure_d": blood_pressure_d,
            "cardiac_frequency": cardiac_frequency,
            "breathing_frequency": breathing_frequency,
            "ventilator": ventilator,
            "o2": o2,
            "o2_saturation": o2_saturation,
            "pafi": pafi,
            "cough": cough,
            "observation": observation,
            "evolution_date": evolution_date
        },
        "medical_study": None,
        "treatment": None,
        "uti": None
    }
    return evolution


def test_login(client):
    data = {
        "username": "chief_med2",
        "password": "chief_med2"
    }
    response = client.post("/auth/login/", data=json.dumps(data), headers={"Content-Type": "application/json"})
    assert "access_token" in response.json


def test_rules(client):
    access_token = get_token(client)
    patient = {
        "medical_coverage": "OSDE 370",
        "other_information": "other",
        "dni": "25462358",
        "first_name": "ricardo",
        "last_name": "sancho",
        "address": "455 la perico",
        "telephone": "+523658563",
        "date_of_birth": "2020-10-04"
    }
    response = client.post(
        "/workforce/patient/",
        data=json.dumps(patient),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    assert 4 == response.json['id']
    internement = {
        "check_in": "2020-10-12",
        "check_out": None,
        "current_disease": "caca",
        "date_diagnosis": "2020-10-12",
        "date_first_simptoms": "2020-10-12",
        "date_of_death": None,
        "medics_ids": [2, 1],
        "patient_id": 4,
        "bed_id": 3,
        "bed_label": None
    }
    response = client.post(
        "/internement/",
        data=json.dumps(internement),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    assert 4 == response.json['internement']['patient']['id']
    assert 2 == response.json['internement']['id']
    evolution = create_evolution()
    response = client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    assert 2 == response.json['evolution']['internement']['id']
    assert 1 == response.json['evolution']['id']
    from notification.models import Notification
    from settings.database import Session
    from workforce.models import Patient
    noti = (
        Session
        .query(Notification)
        .join(Patient)
        .filter(Patient.id == 4)
        .order_by(Notification.id.desc())
        .first()
    )
    assert not noti
    evolution = create_evolution(o2_saturation=90)
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
            .query(Notification)
            .join(Patient)
            .filter(Patient.id == 4)
            .order_by(Notification.id.desc())
            .first()
    )
    assert 'o2_saturation' in noti.normalize_message()
    evolution = create_evolution(drowsiness=True)
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
            .query(Notification)
            .join(Patient)
            .filter(Patient.id == 4)
            .order_by(Notification.id.desc())
            .first()
    )
    assert 'drowsiness' in noti.normalize_message()
    evolution = create_evolution(ventilator='regular')
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
            .query(Notification)
            .join(Patient)
            .filter(Patient.id == 4)
            .order_by(Notification.id.desc())
            .first()
    )
    assert 'ventilator' in noti.normalize_message()
    evolution = create_evolution(breathing_frequency=31)
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
            .query(Notification)
            .join(Patient)
            .filter(Patient.id == 4)
            .order_by(Notification.id.desc())
            .first()
    )
    assert 'breathing_frequency' in noti.normalize_message()
    evolution = create_evolution(o2_saturation=56)
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
            .query(Notification)
            .join(Patient)
            .filter(Patient.id == 4)
            .order_by(Notification.id.desc())
            .first()
    )
    assert 'o2_saturation_compare' in noti.normalize_message()
    evolution = create_evolution(evolution_date="2020-11-01 12:00:00")
    client.post(
        "/evolution/",
        data=json.dumps(evolution),
        headers={"Content-Type": "application/json", "Authorization": f"JWT {access_token}"}
    )
    noti = (
        Session
        .query(Notification)
        .join(Patient)
        .filter(Patient.id == 4)
        .order_by(Notification.id.desc())
        .first()
    )
    assert 'ten_days' in noti.normalize_message()



