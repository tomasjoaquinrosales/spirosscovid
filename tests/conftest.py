import pytest
import os
from app import create_app

env = os.environ.get("FLASK_ENV", 'testing')

@pytest.fixture
def client():
    app = create_app(environment="testing")
    from settings.database import init_db, drop_db
    from command import _create_su, _init_system

    client = app.test_client()
    with app.app_context():
        drop_db()
        init_db()
        _create_su()
        _init_system()
    yield client



