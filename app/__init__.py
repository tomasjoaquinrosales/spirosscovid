from flask import Flask
from flask import render_template
from settings.logger_config import LOGGING
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt
import logging
import logging.config
from os import path, environ
from settings.settings import config
import pusher


# APPLICATION CONFIGURATION

logging.config.dictConfig(LOGGING)

bcrypt = Bcrypt()
jwt = JWTManager()
pusher_client = pusher.Pusher(
    app_id=environ.get("PUSHER_APP_ID"),
    key=environ.get("PUSHER_KEY"),
    secret=environ.get("PUSHER_SECRET"),
    cluster=environ.get("PUSHER_CLUSTER"),
    ssl=True,
)


def create_app(environment="development"):

    app = Flask(__name__, instance_relative_config=True)

    bcrypt.init_app(app)

    env = environ.get("FLASK_ENV", environment)

    app.config.from_object(config[env])

    # jwt = JWTManager(app)
    jwt.init_app(app)

    # MODULES

    with app.app_context():
        from workforce.workforce import workforce
        from authentication.authentication import authentication
        from evolution.evolution import evolution
        from internement.internement import internement
        from system.system import system
        from notification.notification import notification

    @app.route("/")
    def hello_world():
        return render_template("index.html", name="index")

    @app.route("/healthcheck")
    def healthcheck():
        return {"status": "ok"}, 200

    # BLUEPRINT

    app.register_blueprint(authentication, url_prefix="/auth/")
    app.register_blueprint(workforce, url_prefix="/workforce/")
    app.register_blueprint(evolution, url_prefix="/evolution/")
    app.register_blueprint(internement, url_prefix="/internement/")
    app.register_blueprint(system, url_prefix="/system/")
    app.register_blueprint(notification, url_prefix="/notification/")

    # INIT DB WITH CONTEXT
    with app.app_context():
        from settings import database

        database.init_db()

    from settings.database import Session

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        Session.remove()

    return app
