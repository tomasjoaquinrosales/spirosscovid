from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean, Enum
from sqlalchemy.orm import relationship
from utils.models import DateAware
from internement.models import internements_medics

# Create your model here.


class User(DateAware):
    __tablename__ = "users"

    username = Column(String(100), unique=True, nullable=False)
    password = Column(String(500), nullable=False)
    is_superuser = Column(Boolean, default=False, nullable=False)
    is_medic = Column(Boolean, default=False, nullable=False)
    manager = relationship(
        "Manager", uselist=False, back_populates="user", cascade="all, delete"
    )
    medic = relationship(
        "Medic", uselist=False, back_populates="user", cascade="all, delete"
    )
    user_profile = relationship(
        "UserProfile", uselist=False, back_populates="user", cascade="all, delete"
    )

    def __init__(
        self, username, password, is_superuser=False, is_manager=False, is_medic=False
    ):
        self.username = username
        self.password = password
        self.is_superuser = is_superuser
        self.is_medic = is_medic

    def __repr__(self):
        return f"<User {self.username}>"


class UserProfile(DateAware):
    __tablename__ = "userprofiles"

    dni = Column(String(8), unique=True, nullable=False)
    first_name = Column(String(150), nullable=False)
    last_name = Column(String(150), nullable=False)
    address = Column(String(150), nullable=False)
    telephone = Column(String(150), nullable=False)
    date_of_birth = Column(DateTime, nullable=False)
    user_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    )
    user = relationship("User", back_populates="user_profile", cascade="all, delete")

    def __init__(
        self, dni, first_name, last_name, user_id, address, telephone, date_of_birth
    ):
        self.dni = dni
        self.first_name = first_name
        self.last_name = last_name
        self.user_id = user_id
        self.address = address
        self.telephone = telephone
        self.date_of_birth = date_of_birth

    def __repr__(self):
        return f"<UserProfile {self.dni}>"


class Manager(DateAware):
    __tablename__ = "managers"

    user_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    )
    user = relationship("User", back_populates="manager", cascade="all, delete")

    def __init__(self, user_id):
        self.user_id = user_id

    def __repr__(self):
        return f"<Manager {self.id}>"


class Medic(DateAware):
    __tablename__ = "medics"

    docket = Column(Integer, unique=True, nullable=False)
    user_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    )
    user = relationship("User", back_populates="medic", cascade="all, delete")
    system_id = Column(Integer, ForeignKey("systems.id", ondelete="CASCADE"))
    system = relationship("System", back_populates="medics", cascade="all, delete")
    evolution = relationship("Evolution", lazy="subquery", cascade="all, delete")
    internements = relationship(
        "Internement",
        secondary=internements_medics,
        back_populates="medic",
        cascade="all, delete",
    )
    notifications = relationship(
        "Notification", back_populates="medic", cascade="all, delete"
    )

    def __init__(self, user_id, docket):
        self.user_id = user_id
        self.docket = docket

    def __repr__(self):
        return f"<Medic {self.id}>"

    def __init__(self, user_id, docket, system_id, is_chief=False):
        self.user_id = user_id
        self.docket = docket
        self.is_chief = is_chief
        self.system_id = system_id

    def __repr__(self):
        return f"<Medic {self.id}>"

    @classmethod
    def get_by_username(cls, username):
        from system.models import System

        system = None
        medic = None
        medic = cls.query.join(User).filter(User.username == username).first()
        if medic:
            system = System.query.filter_by(id=medic.system_id).first()
        return medic, system


class Patient(DateAware):
    __tablename__ = "patients"

    medical_coverage = Column(String(250))
    other_information = Column(String(250))
    dni = Column(String(8), unique=True, nullable=False)
    first_name = Column(String(150), nullable=False)
    last_name = Column(String(150), nullable=False)
    address = Column(String(150), nullable=False)
    telephone = Column(String(150), nullable=False)
    date_of_birth = Column(DateTime, nullable=False)
    contact = relationship(
        "Contact", back_populates="patient", cascade="all, delete", passive_deletes=True
    )
    internement = relationship(
        "Internement", back_populates="patient", cascade="all, delete"
    )

    bed_id = Column(Integer, ForeignKey("beds.id", ondelete="CASCADE"))
    bed = relationship("Bed", back_populates="patient", cascade="all, delete")

    notifications = relationship(
        "Notification", back_populates="patient", cascade="all, delete"
    )

    def __init__(
        self,
        medical_coverage,
        other_information,
        dni,
        first_name,
        last_name,
        address,
        telephone,
        date_of_birth,
    ):
        self.dni = dni
        self.first_name = first_name
        self.last_name = last_name
        self.address = address
        self.telephone = telephone
        self.date_of_birth = date_of_birth
        self.medical_coverage = medical_coverage
        self.other_information = other_information

    def __repr__(self):
        return f"<Patient {self.id}>"


class Contact(DateAware):
    __tablename__ = "contacts"

    patient_id = Column(Integer, ForeignKey("patients.id", ondelete="CASCADE"))
    patient = relationship("Patient", back_populates="contact")
    first_name = Column(String(150))
    last_name = Column(String(150))
    telephone = Column(String(150))

    def __init__(self, patient_id, first_name, last_name, telephone):
        self.patient_id = patient_id
        self.first_name = first_name
        self.last_name = last_name
        self.telephone = telephone

    def __repr__(self):
        return f"<Contact {self.id}>"
