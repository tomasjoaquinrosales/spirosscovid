from flask_jwt_extended import get_jwt_identity
from sqlalchemy.exc import DatabaseError, IntegrityError
from marshmallow import ValidationError

from internement.models import Internement, InternementSystem
from system.models import Bed, Room, System
from system.serializers import RoomSchema, BedSchema
from workforce.models import User, UserProfile, Medic, Patient, Contact, Manager

from .serializers import (
    ContactSchema,
    UserSchema,
    UserProfileSchema,
    MedicSchema,
    PatientSchema,
    ManagerSchema,
)
from utils.errors import ConflictError, NotFoundError, ClientException
from utils.responses import (
    AbstractResponse,
    GenericCreateResponse,
    GenericListResponse,
    GenericRetrieveResponse,
    GenericDeleteResponse,
)
from sqlalchemy import or_, and_, exists
from utils.serializers import ImageSchema, YesOrNotResponseSchema
from utils.images_helpers import save_to_image
from flask_bcrypt import generate_password_hash
from app import bcrypt
from http import HTTPStatus
from sqlalchemy import event
from settings.database import Session, engine


# Create your responses here.


class UserResponse(GenericCreateResponse, GenericRetrieveResponse, GenericListResponse):
    model = User
    schema = UserSchema
    session = Session

    def pre_validation(self):
        user = User.query.filter_by(username=self.data["username"]).first()

        if user:
            raise ConflictError(user_err_msg=f"{user} already exists.")

    def create_instance(self):
        user = User(**self.data)

        password_crypt = bcrypt.generate_password_hash(user.password, 10).decode(
            "utf-8"
        )

        user.password = password_crypt

        return user

    def lists(self, *args, **kwargs):
        page_size = kwargs["page_size"]
        page = kwargs["page"]

        page_from = (page - 1) * page_size

        subquery_medic = (
            ~self.session.query(Medic)
            .filter(Medic.user_id == UserProfile.user_id)
            .exists()
        )
        # subquery_patient = ~self.session.query(Patient).filter(
        #     Patient.user_id == UserProfile.user_id).exists()

        users = (
            self.session.query(UserProfile)
            .filter(subquery_medic)
            .filter(UserProfile.created.between(kwargs["from"], kwargs["to"]))
            .order_by(UserProfile.id.asc())
            .limit(page_size)
            .offset(page_from)
        )

        total_results = (
            UserProfile.query.filter(subquery_medic)
            .filter(UserProfile.created.between(kwargs["from"], kwargs["to"]))
            .count()
        )

        response = dict()
        list_serializer = list()

        if users:
            for obj in users:
                serializer = UserProfileSchema().dump(obj)
                list_serializer.append(serializer)

        response["status"] = HTTPStatus.OK
        response["totalResults"] = total_results
        response["results"] = list_serializer
        return response, HTTPStatus.OK


class UserProfileResponse(GenericRetrieveResponse, GenericCreateResponse):
    model = UserProfile
    schema = UserProfileSchema
    session = Session

    def pre_validation(self, user_id):

        if self.data["user_id"] != user_id:
            raise ConflictError(
                user_err_msg=f"{user_id} not math with user_id {user_id} url param."
            )

        user = User.query.filter_by(id=user_id).first()

        if not user:
            raise ConflictError(user_err_msg=f"User({user_id}) does not exists.")

        user_profile = (
            UserProfile.query.join(UserProfile.user).filter(User.id == user.id).first()
        )

        if user_profile:
            raise ConflictError(user_err_msg=f"{user_profile} already exists.")


class UserImageResponse(GenericRetrieveResponse, GenericCreateResponse):
    model = User
    schema = ImageSchema
    session = Session

    def create(self, blueprint_name, id):

        serializer = self.pre_create()

        user = self.get_object(id)

        image_name = save_to_image(
            blueprint_name,
            image_file=serializer["thumbnail"],
            extension=serializer["extension"],
        )

        user.thumbnail = image_name

        self.session.commit()

        deserializer = UserSchema().dump(user)

        return deserializer, HTTPStatus.OK


class ManagerResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
):
    model = Manager
    schema = ManagerSchema
    session = Session

    def pre_validation(self, user_id):

        user_id_json = self.data["user_id"]
        if user_id_json != user_id:
            raise ConflictError(
                user_err_msg=f"{user_id_json} not math with user_id {user_id} url param."
            )

        user = User.query.filter_by(id=user_id).first()

        if not user:
            raise ConflictError(user_err_msg=f"User({user}) does not exists.")

        manager_by_user = (
            Manager.query.join(Manager.user).filter(User.id == user.id).first()
        )

        if manager_by_user:
            raise ConflictError(user_err_msg=f"Already exists a manager to the {user}")


class MedicResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
):
    model = Medic
    schema = MedicSchema
    session = Session

    """def pre_validation(self, user_id):

        user_id_json = self.data["user_id"]
        if user_id_json != user_id:
            raise ConflictError(
                user_err_msg=f"{user_id_json} not math with user_id {user_id} url param."
            )

        user = User.query.filter_by(id=user_id).first()

        if not user:
            raise ConflictError(user_err_msg=f"User({user}) does not exists.")

        medic_by_user = Medic.query.join(Medic.user).filter(User.id == user.id).first()

        if medic_by_user:
            raise ConflictError(user_err_msg=f"Already exists a medic to the {user}")"""

    def lists(self):
        from utils.blacklist_helpers import get_system_from_user
        from flask_jwt_extended import get_jwt_identity

        username = get_jwt_identity()["username"]

        return super().lists(system_id=get_system_from_user(username).id)


class MedicFilterResponse(GenericRetrieveResponse):
    model = Medic
    schema = MedicSchema
    session = Session

    def retrieve(self, username: str):
        user = User.query.filter_by(username=username).first()
        if not user:
            raise ConflictError(user_err_msg=f"User({username}) does not exists.")
        medic = Medic.query.filter(Medic.user == user).first()
        if not medic:
            raise ConflictError(user_err_msg=f"Medic does not exists.")
        serializer = self.schema().dump(medic)
        return serializer, HTTPStatus.OK


class PatientResponse(
    GenericCreateResponse,
    GenericRetrieveResponse,
    GenericListResponse,
    GenericDeleteResponse,
):
    model = Patient
    schema = PatientSchema
    session = Session

    def lists(self, *args, **kwargs):
        page_size = kwargs["page_size"]
        page = kwargs["page"]
        filt = kwargs["patient"]

        if filt:
            filt = f"%{filt}%"
        else:
            filt = "%"

        page_from = (page - 1) * page_size
        page_to = page_size * page

        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)

        final_list = []

        patients = (
            self.session.query(Patient)
            .join(Internement)
            .join(Bed, isouter=True)
            .join(Room, isouter=True)
            .filter(
                Patient.first_name.ilike(filt)
                | Patient.last_name.ilike(filt)
                | Room.name.ilike(filt)
                | Bed.label.ilike(filt)
            )
            .all()
        )

        for patient in patients:
            last_internement = (
                self.session.query(Internement)
                .filter(Internement.patient_id == patient.id)
                .filter(
                    Internement.date_of_death.is_(None)
                    & Internement.check_out.is_(None)
                )
                .order_by(Internement.id.desc())
                .first()
            )
            if last_internement:
                last_transfer = (
                    self.session.query(InternementSystem)
                    .filter(InternementSystem.internement_id == last_internement.id)
                    .order_by(InternementSystem.id.desc())
                    .first()
                )
                # si sos piso covid, te muestra hotel y domicilio, sino los de tu sistema
                if (
                    last_transfer.system_id > 3 and system.id == 3
                ) or last_transfer.system_id == system.id:
                    dump = dict()
                    dump = PatientSchema().dump(patient)
                    dump["is_int_active"] = True
                    dump["is_dead"] = False
                    final_list.append(dump)
        """
        active_patients = (
            self.session.query(Patient)
            .join(Internement)
            .join(InternementSystem)
            .join(Bed)
            .join(Room)
            .filter(
                Internement.date_of_death.is_(None) & Internement.check_out.is_(None)
            )
            .filter(InternementSystem.system_id == system.id)
            .filter(
                Patient.first_name.ilike(filt)
                | Patient.last_name.ilike(filt)
                | Room.name.ilike(filt)
                | Bed.label.ilike(filt)
            )
            .all()
        )"""

        if system.name == "Guardia":
            for patient in patients:
                last_internement = (
                    self.session.query(Internement)
                    .filter(Internement.patient_id == patient.id)
                    .order_by(Internement.id.desc())
                    .first()
                )
                if last_internement.date_of_death or last_internement.check_out:
                    dump = dict()
                    dump = PatientSchema().dump(patient)
                    if last_internement.date_of_death:
                        dump["is_dead"] = True
                    else:
                        dump["is_dead"] = False
                    dump["is_int_active"] = False
                    final_list.append(dump)

            """
            inactive_patients = (
                self.session.query(Patient)
                .join(Internement)
                .join(InternementSystem)
                .filter(
                    Internement.date_of_death.isnot(None)
                    | Internement.check_out.isnot(None)
                )
                .filter(InternementSystem.system_id == system.id)
                .filter(Patient.first_name.ilike(filt) | Patient.last_name.ilike(filt))
                .all()
            )
            """
            not_internement = (
                Patient.query.filter_by(internement=None)
                .filter(Patient.first_name.ilike(filt) | Patient.last_name.ilike(filt))
                .all()
            )
            """
            for inactive in inactive_patients:
                if not inactive in active_patients:
                    dump = dict()
                    dump = PatientSchema().dump(inactive)
                    dump["is_int_active"] = False
                    final_list.append(dump)
            """
            for not_int in not_internement:
                dump = dict()
                dump = PatientSchema().dump(not_int)
                dump["is_int_active"] = None
                dump["is_dead"] = False
                final_list.append(dump)

        total_results = len(final_list)

        response = dict()

        response["status"] = HTTPStatus.OK
        response["totalResults"] = total_results
        response["results"] = final_list[page_from:page_to]
        return response, HTTPStatus.OK

    def retrieve(self, patient_id: int):
        patient = self.model.query.filter_by(id=patient_id).first()
        serialized = dict()
        serialized = self.schema().dump(patient)
        internement = (
            self.session.query(Internement)
            .filter(Internement.patient_id == patient.id)
            .order_by(Internement.id.desc())
            .first()
        )
        if internement and internement.date_of_death:
            serialized["is_dead"] = True
        else:
            serialized["is_dead"] = False
        bed = patient.bed
        if bed:
            room = bed.room
            serialized["room"] = RoomSchema().dump(room)
        return serialized, HTTPStatus.OK


class ContactResponse(
    GenericCreateResponse, GenericRetrieveResponse, GenericDeleteResponse
):
    model = Contact
    schema = ContactSchema
    session = Session

    def pre_validation(self, user_id):

        user_id_json = self.data["user_id"]
        if user_id_json != user_id:
            raise ConflictError(
                user_err_msg=f"{user_id_json} not math with user_id {user_id} url param."
            )

        patient = Patient.query.filter_by(id=patient_id).first()

        if not patient:
            raise ConflictError(user_err_msg=f"Patient({patient_id}) does not exists.")


class ContactListResponse(GenericListResponse):
    model = Contact
    schema = ContactSchema
    session = Session

    def get_queryset(self, *args, **kwargs):

        page_size = kwargs["page_size"]
        page = kwargs["page"]

        page_from = (page - 1) * page_size

        queryset = (
            Contact.query.filter(Contact.is_active.is_(kwargs["is_active"]))
            .filter(Contact.patient_id == kwargs["patient_id"])
            .filter(Contact.created.between(kwargs["from"], kwargs["to"]))
            .order_by(Contact.id.asc())
            .limit(page_size)
            .offset(page_from)
        )

        total_results = (
            Contact.query.filter(Contact.is_active.is_(kwargs["is_active"]))
            .filter(Contact.patient_id == kwargs["patient_id"])
            .filter(Contact.created.between(kwargs["from"], kwargs["to"]))
            .count()
        )

        return queryset, total_results


class UserValidationsResponse:
    def __init__(self, model_name):
        self.model = self._get_model_by_name(model_name)

    def _get_model_by_name(self, model_name):
        return {
            "patient": Patient,
            "user": UserProfile,
        }.get(model_name.lower())

    def dispatch(self, field, value):
        if field == "dni":
            return self.validate_dni(value)
        if field == "username":
            return self.validate_username(value)
        if field == "docket":
            return self.validate_docket(value)
        return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK

    def validate_dni(self, value):
        user_profile = self.model.query.filter_by(dni=value).first()
        if not user_profile:
            return YesOrNotResponseSchema().dump({"valid": True}), HTTPStatus.OK
        return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK

    def validate_username(self, value):
        user = User.query.filter_by(username=value).first()
        if not user:
            return YesOrNotResponseSchema().dump({"valid": True}), HTTPStatus.OK
        return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK

    def validate_docket(self, value):
        medic = Medic.query.filter_by(docket=value).first()
        if not medic:
            return YesOrNotResponseSchema().dump({"valid": True}), HTTPStatus.OK
        return YesOrNotResponseSchema().dump({"valid": False}), HTTPStatus.OK
