from marshmallow import (
    fields,
    validate,
    pre_dump,
    validates,
    ValidationError,
    validates_schema,
)
from utils.serializers import BaseSchema
from workforce.models import User, Patient, Medic
from utils.enums import FamilyRelationEnum
from system.models import System

# Create your serializers here.


class PatientSchema(BaseSchema):
    medical_coverage = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    other_information = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    dni = fields.Str(required=True, allow_none=False, validate=validate.Length(equal=8))
    first_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    last_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    address = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    telephone = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    date_of_birth = fields.DateTime(required=True, allow_none=False, format="%Y-%m-%d")

    bed = fields.Nested("BedSchema", dump_only=True)

    @validates("dni")
    def validate_dni(self, value):
        patient = Patient.query.filter_by(dni=value, is_active=True).first()
        if patient:
            raise ValidationError(f"Already exists a patient with the dni {value}")

    @validates_schema
    def validate_medic_system(self, data, **kwargs):
        from flask_jwt_extended import get_jwt_identity

        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)
        if not system or system.name != "Guardia":
            raise ValidationError(
                f"Attempting to create Patient from outside 'Guardia'"
            )


class ContactSchema(BaseSchema):
    patient_id = fields.Int(required=True)
    patient = fields.Nested(lambda: PatientSchema(exclude=["created", "modified"]))
    first_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    last_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    family_relation = fields.Str(
        required=True,
        validate=validate.OneOf(
            [FamilyRelationEnum(name).name for name in FamilyRelationEnum]
        ),
    )
    telephone = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )

    @pre_dump
    def add_family_relation(self, data, many, **kwargs):
        data.family_relation = data.family_relation.value
        return data

    @validates("patient_id")
    def validate_patient_id(self, value):
        patient = Patient.query.filter_by(id=value, is_active=True).first()
        if not patient:
            raise ValidationError("Patient does not exist")


class MedicSchema(BaseSchema):
    user_id = fields.Int(required=True, load_only=True)
    user = fields.Nested(
        lambda: UserSchema(exclude=["created", "modified"]), dump_only=True
    )
    is_chief = fields.Boolean(default=False)
    docket = fields.Int(required=True, allow_none=False)
    system_id = fields.Int(required=True, allow_none=False)

    @validates("user_id")
    def validate_user_id(self, value):
        user = User.query.filter_by(id=value, is_active=True).first()
        if not user:
            raise ValidationError("User does not exist")

    @validates("system_id")
    def validate_system_id(self, value):
        system = System.query.filter_by(id=value, is_active=True).first()
        if not system:
            raise ValidationError("System does not exist")

    @validates_schema
    def validate_ralationship(self, data, **kwargs):
        medic_by_user = (
            Medic.query.join(Medic.user).filter(User.id == data["user_id"]).first()
        )
        if medic_by_user:
            raise ValidationError(f"Already exists a medic with the user provided")


class UserProfileSchema(BaseSchema):
    user_id = fields.Int(required=True)
    user = fields.Nested(lambda: UserSchema(exclude=["created", "modified"]))
    dni = fields.Str(required=True, allow_none=False, validate=validate.Length(equal=8))
    first_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    last_name = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    address = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    telephone = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    date_of_birth = fields.DateTime(required=True, allow_none=False, format="%Y-%m-%d")

    @validates("user_id")
    def validate_user_id(self, value):
        user = User.query.filter_by(id=value, is_active=True).first()
        if not user:
            raise ValidationError("User does not exist")


class ManagerSchema(BaseSchema):
    user_id = fields.Int(required=True)
    user = fields.Nested(lambda: UserSchema(exclude=["created", "modified"]))

    @validates("user_id")
    def validate_user_id(self, value):
        user = User.query.filter_by(id=value, is_active=True).first()
        if not user:
            raise ValidationError("User does not exist")


# TODO falta dumpear el id
# TODO falta dump only los booleanos
class UserSchema(BaseSchema):
    username = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    password = fields.Str(
        required=True, load_only=True, validate=validate.Length(min=5)
    )
    is_superuser = fields.Boolean(default=False)
    is_manager = fields.Boolean(default=False)
    is_medic = fields.Boolean(default=True)
    is_active = fields.Boolean(default=True)
    thumbnail = fields.Str(dump_only=True)
