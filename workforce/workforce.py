from flask import Blueprint, request
from utils.decorators import ErrorHandler
from workforce.responses import (
    UserResponse,
    UserProfileResponse,
    UserImageResponse,
    UserValidationsResponse,
    ContactResponse,
    ContactListResponse,
    MedicResponse,
    MedicFilterResponse,
    PatientResponse,
)
from flask_jwt_extended import get_jwt_claims, jwt_required
from authentication.permissions import admin_required, chief_required, staff_required
from utils.requests import get_url_params
from flask_cors import CORS
import logging


workforce = Blueprint(
    "workforce", __name__, template_folder="templates", static_folder="static"
)

CORS(workforce)

logger = logging.getLogger("app")


# Create your end-points here.


# Create a User


@workforce.route("user/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, workforce)
@admin_required
def add_user():
    response, status_code = UserResponse(request).create()
    return response, status_code


# Get User By Id


@workforce.route("user/<int:user_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@admin_required
def get_user(user_id):
    response, status_code = UserResponse(request).retrieve(user_id)
    return response, status_code


# Get User List


@workforce.route("user/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def get_users():
    filters = get_url_params(request)
    response, status_code = UserResponse(request).lists(**filters)
    return response, status_code


# Create a User Profile


@workforce.route("user-profile/<int:user_id>/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, workforce)
@admin_required
def add_user_profile(user_id):
    response, status_code = UserProfileResponse(request).create(user_id)
    return response, status_code


#####################
# MEDIC #############
#####################


# Create a Medic


@workforce.route("medic/<int:user_id>/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def add_medic(user_id):
    response, status_code = MedicResponse(request).create(user_id)
    return response, status_code


# Get Medic By Id


@workforce.route("medic/<int:medic_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def get_medic(medic_id):
    response, status_code = MedicResponse(request).retrieve(medic_id)
    return response, status_code


# Get MEdic By Username


@workforce.route("medic/<username>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def get_medic_by_username(username):
    response, status_code = MedicFilterResponse().retrieve(username)
    return response, status_code


# Get Medic List


@workforce.route("medic/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def get_medics():
    response, status_code = MedicResponse(request).lists()
    return response, status_code


# Delete Medic By ID


@workforce.route("medic/<int:medic_id>/", methods=["DELETE"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def delete_medic(medic_id):
    response, status_code = MedicResponse(request).delete(medic_id)
    return response, status_code


#####################
# PATIENT ###########
#####################


# Create a Patient


@workforce.route("patient/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def add_patient():
    response, status_code = PatientResponse(request).create()
    return response, status_code


# Get Patient By Id


@workforce.route("patient/<int:patient_id>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def get_patient(patient_id):
    response, status_code = PatientResponse(request).retrieve(patient_id)
    return response, status_code


# Get Patient List


@workforce.route("patient/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def get_patients():
    filters = get_url_params(request)
    response, status_code = PatientResponse(request).lists(**filters)
    return response, status_code


# Delete Patient By ID


@workforce.route("patient/<int:patient_id>/", methods=["DELETE"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def delete_patient(patient_id):
    response, status_code = PatientResponse(request).delete(patient_id)
    return response, status_code


#####################
# CONTACT ###########
#####################


# Create a Contact to a Patient


@workforce.route("patient/<int:patient_id>/contact/", methods=["POST"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def add_contact(patient_id):
    response, status_code = ContactResponse(request).create(patient_id)
    return response, status_code


# Get Patient List


@workforce.route("patient/<int:patient_id>/contact/list/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@chief_required
def get_contacts_by_patient_id(patient_id):
    filters = get_url_params(request)
    filters["patient_id"] = patient_id
    response, status_code = ContactListResponse(request).lists(**filters)
    return response, status_code


# Upload a Image a Product By Id


# @workforce.route('/user/<int:user_id>/image', methods=['POST'])
# @jwt_required
# @ErrorHandler(logger, workforce)
# @chief_required
# def upload_image(user_id):
#     response, status_code = UserImageResponse(
#         request).create(workforce.name, user_id)
#     return response, status_code


################
# VALIDATIONS ##


@workforce.route("user/valid/<field>/<user_identify>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def user_validation(field, user_identify):
    response, status_code = UserValidationsResponse("user").dispatch(
        field, user_identify
    )
    return response, status_code


@workforce.route("patient/valid/<field>/<user_identify>/", methods=["GET"])
@jwt_required
@ErrorHandler(logger, workforce)
@staff_required
def patient_validation(field, user_identify):
    response, status_code = UserValidationsResponse("patient").dispatch(
        field, user_identify
    )
    return response, status_code
