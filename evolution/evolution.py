from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from authentication.permissions import (
    admin_required,
    chief_required,
    staff_required
)
from utils.decorators import ErrorHandler
from evolution.responses import EvolutionResponse, EvolutionPatientResponse
from utils.requests import get_url_params
import logging
from flask_cors import CORS


evolution = Blueprint('evolution', __name__,
                      template_folder='templates', static_folder='static')

CORS(evolution)

logger = logging.getLogger('app')


# Create your end-points here.


@evolution.route('', methods=['POST'])
@jwt_required
@ErrorHandler(logger, evolution)
@staff_required
def add_evolution():
    response, status_code = EvolutionResponse(request).create()
    return response, status_code


@evolution.route('<int:evolution_id>/', methods=['GET'])
@jwt_required
@ErrorHandler(logger, evolution)
@staff_required
def get_evolution(evolution_id):
    response, status_code = EvolutionResponse(request).retrieve(evolution_id)
    return response, status_code


@evolution.route('list/', methods=['GET'])
@jwt_required
@ErrorHandler(logger, evolution)
@staff_required
def evolution_list():
    filters = get_url_params(request)
    response, status_code = EvolutionResponse(request).lists(**filters)
    return response, status_code


@evolution.route('list_evo_change/<int:patient_id>', methods=['GET'])
@jwt_required
@ErrorHandler(logger, evolution)
@staff_required
def list_evo_change(patient_id):
    response, status_code = EvolutionResponse(request).list_evolution_change_internement(patient_id)
    return response, status_code


@evolution.route('patient/<int:patient_id>/', methods=['GET'])
@jwt_required
@ErrorHandler(logger, evolution)
@staff_required
def get_evolution_by_patient_id(patient_id):
    response, status_code = EvolutionPatientResponse().retrieve(patient_id)
    return response, status_code


# VALIDATIONS
