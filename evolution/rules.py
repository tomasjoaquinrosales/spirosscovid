from datetime import datetime

import formulas
from business_rule_engine import RuleParser

from internement.models import Internement
from notification.models import Notification
from notification.responses import NotificationResponse
from workforce.models import Patient
from settings.database import Session as session


class RuleParam:
    drowsiness = None
    ventilator = None
    breathing_frequency = None
    today = None
    o2_saturation = None
    o2_saturation_last = None

    def __init__(self, drowsiness: bool, ventilator: str, breathing_frequency: int, evolution_date: str, today: str,
                 o2_saturation: int, o2_saturation_last: int):
        self.drowsiness = drowsiness
        self.ventilator = ventilator
        self.breathing_frequency = breathing_frequency
        self.evolution_date = evolution_date
        self.today = today
        self.o2_saturation = o2_saturation
        self.o2_saturation_last = o2_saturation_last

    def to_json(self):
        return {
            'drowsiness': self.drowsiness,
            'ventilator': self.ventilator,
            'breathing_frequency': self.breathing_frequency,
            'evolution_date': self.evolution_date,
            'today': self.today,
            'o2_saturation': self.o2_saturation,
            'o2_saturation_last': self.o2_saturation_last
        }


class RuleParserHook(RuleParser):

    def execute(self, params, stop_on_first_trigger: bool = True):
        rule_was_triggered = False
        for rule_name, rule in self.rules.items():

            condition_compiled = self._compile_condition(rule['condition'])
            params_condition = self._get_params(params, condition_compiled)
            rvalue_conditions = condition_compiled(**params_condition).tolist()
            if self.codition_requires_bool and not isinstance(rvalue_conditions, bool):
                raise ValueError('rule: {} - condition does not return a boolean value!'.format(rule_name))

            if rvalue_conditions:
                rule_was_triggered = True

                action_compiled = self._compile_condition(rule['action'])
                params_actions = self._get_params(params, action_compiled)
                rvalue_action = action_compiled(**params_actions)

                if stop_on_first_trigger:
                    break
        return rule_was_triggered


class Rules:
    FUNCTIONS = None
    date_format = "%d/%m/%Y"
    rules = None
    parser = RuleParserHook()
    drowsiness = None
    ventilator_options = None
    breathing_frequency = None
    days_dif = None
    saturation = None
    saturation_compare = None
    response = dict()

    def __init__(self,
                 drowsiness: bool = True,
                 ventilator_options=None,
                 breathing_frequency: int = 30,
                 days_dif: int = 10,
                 saturation: int = 92,
                 saturation_compare: int = 3
                 ):
        if ventilator_options is None:
            ventilator_options = ["regular", "bad"]
        self.drowsiness = drowsiness
        self.ventilator_options = ventilator_options
        self.breathing_frequency = breathing_frequency
        self.days_dif = days_dif
        self.saturation = saturation
        self.saturation_compare = saturation_compare
        self._set_functions()
        self._set_rules()
        self._set_parser_rule()

    def _set_functions(self):
        self.FUNCTIONS = formulas.get_functions()
        self.FUNCTIONS['FUNC_VENTILATOR'] = lambda x: x in self.ventilator_options
        self.FUNCTIONS['FUNC_DAYS_DIFF'] = lambda x, y: (
                datetime.strptime(x, self.date_format) - datetime.strptime(y, self.date_format)).days
        self.FUNCTIONS['FUNC_SATURATION_COMP'] = lambda saturation_t, saturation_y: 0 if saturation_t >= self.saturation else 100 - (
                saturation_t * 100 / saturation_y)

    def _set_rules(self):
        self.rules = f"""
        rule "drowsiness"
        when
            drowsiness = {self.drowsiness}
        then
            has_drowsiness(drowsiness)
        end
        rule "ventilator"
        when
            FUNC_VENTILATOR(ventilator) = True
        then
            has_good_mechanic_ventilatory(ventilator)
        end
        rule "breathing_frequency"
        when
            breathing_frequency > {self.breathing_frequency}
        then
            has_normal_breathing_frequency(breathing_frequency)
        end
        rule "evolution_date"
        when
            FUNC_DAYS_DIFF(today, evolution_date) >= {self.days_dif}
        then
            has_passed_ten_days(evolution_date, today)
        end
        rule "o2_saturation"
        when
            o2_saturation <= {self.saturation}
        then
            has_normal_o2_saturation(o2_saturation)
        end
        rule "saturation compare"
        when
            FUNC_SATURATION_COMP(o2_saturation, o2_saturation_last) >= {self.saturation_compare}
        then
            o2_saturation_compare(o2_saturation, o2_saturation_last)
        end
        """

    def _set_parser_rule(self):
        self.parser.register_function(self.has_drowsiness)
        self.parser.register_function(self.has_good_mechanic_ventilatory)
        self.parser.register_function(self.has_normal_breathing_frequency)
        self.parser.register_function(self.has_passed_ten_days)
        self.parser.register_function(self.has_normal_o2_saturation)
        self.parser.register_function(self.o2_saturation_compare)
        self.parser.parsestr(self.rules)

    def has_drowsiness(self, drowsiness):
        message = f'Somnolencia:evaular pase a UTI.'
        print(message)
        self.response['drowsiness'] = message
        return message

    def has_good_mechanic_ventilatory(self, ventilator):
        message = f'Mecánica Ventilatoria ({ventilator}):evaular pase a UTI.'
        print(message)
        self.response['ventilator'] = message
        return message

    def has_normal_breathing_frequency(self, breathing_frequency):
        message = f'Frecuencia Respiratoria > ({breathing_frequency}):evaular pase a UTI.'
        print(message)
        self.response['breathing_frequency'] = message
        return message

    def has_passed_ten_days(self, evolution_date, today):
        date_format = "%d/%m/%Y"
        quantity_day = (datetime.strptime(today, date_format) - datetime.strptime(evolution_date, date_format)).days
        message = f'Han pasado {quantity_day} días desde el inicio de los síntomas:evaular ALTA.'
        print(message)
        self.response['ten_days'] = message
        return message

    def has_normal_o2_saturation(self, o2_saturation):
        message = f'Saturación de oxígeno < a {o2_saturation}:evaular oxigenoterapia y prono.'
        print(message)
        self.response['o2_saturation'] = message
        return message

    def o2_saturation_compare(self, o2_saturation, o2_saturation_last):
        o2_saturation_percentage = o2_saturation_last * 100 / o2_saturation
        dif = 100 - o2_saturation_percentage
        message = f'Saturación de oxígeno bajo {round(dif, 0)}% respecto a la evolución anterior:evaular oxigenoterapia y prono.'
        print(message)
        self.response['o2_saturation_compare'] = message
        return message

    def send_notifications(self, patient_id):
        internement = session.query(Internement) \
            .join(Patient) \
            .filter(Internement.check_out.is_(None)) \
            .filter(Patient.id == patient_id) \
            .first()
        medics = internement.medic
        for medic in medics:
            message = ''
            for key in self.response:
                message = message + f'{key}:{self.response[key]} \n'
            n = Notification(medic.id, patient_id, message)
            session.add(n)
            session.commit()
        NotificationResponse.send_pusher_notification(self.response, patient_id)

    def check_evolution(self, instance: RuleParam, patient_id):
        self.parser.execute(instance.to_json(), False)
        if self.response:
            self.send_notifications(patient_id)
