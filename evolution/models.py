from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    ForeignKey,
    DateTime,
    Table,
    Boolean,
)
from sqlalchemy.orm import relationship
from settings.database import Base
from utils.models import BaseMixin, DateAware
import datetime

# Create your model here.


class UTIInformation(DateAware):
    __tablename__ = "utiinformations"

    arm = Column(Boolean, nullable=False)
    arm_description = Column(String(150), nullable=True)
    tracheostomy = Column(Boolean, nullable=False)
    vasopressors = Column(Boolean, nullable=False)
    vasopressors_description = Column(String(150), nullable=True)
    evolution_template_id = Column(
        Integer, ForeignKey("evolutiontemplates.id", ondelete="CASCADE")
    )
    evolution_template_uti = relationship(
        "EvolutionTemplate", back_populates="uti", cascade="all, delete"
    )

    def __repr__(self):
        return f"<UTIInformation {self.id}>"


class Treatment(DateAware):
    __tablename__ = "treatments"

    name = Column(String(150), nullable=False)
    description = Column(String(150), nullable=True)
    evolution_template_id = Column(
        Integer, ForeignKey("evolutiontemplates.id", ondelete="CASCADE")
    )
    evolution_template_t = relationship(
        "EvolutionTemplate", back_populates="treatment", cascade="all, delete"
    )

    def __repr__(self):
        return f"<Treatment {self.id}>"


class MedicalStudy(DateAware):
    __tablename__ = "medicalstudies"

    name = Column(String(150), nullable=False)
    is_normal = Column(Boolean, nullable=False)
    is_pathological = Column(Boolean, nullable=False)
    description = Column(String(150), nullable=True)
    evolution_template_id = Column(
        Integer, ForeignKey("evolutiontemplates.id", ondelete="CASCADE")
    )
    evolution_template_ms = relationship(
        "EvolutionTemplate", back_populates="medical_study", cascade="all, delete"
    )

    def __repr__(self):
        return f"<MedicalStudy {self.id}>"


class EvolutionTemplate(BaseMixin, Base):
    # TODO volar evolution_date
    __tablename__ = "evolutiontemplates"

    created = Column(DateTime, default=datetime.datetime.now())
    modified = Column(DateTime, default=datetime.datetime.now())
    evolution_date = Column(DateTime, default=datetime.datetime.today(), nullable=False)
    temperature = Column(Integer, nullable=False)
    blood_pressures_s = Column(Integer, nullable=False)
    blood_pressure_d = Column(Integer, nullable=False)
    cardiac_frequency = Column(Integer, nullable=False)
    breathing_frequency = Column(Integer, nullable=False)
    ventilator = Column(String(150), nullable=True)
    is_o2_required = Column(Boolean, default=False, nullable=False)
    o2 = Column(String(150), nullable=True)
    o2_saturation = Column(Integer, nullable=False)
    is_pafi_required = Column(Boolean, default=False, nullable=False)
    pafi = Column(Integer, nullable=True)
    prono_vigil = Column(Boolean, default=False, nullable=False)
    cough = Column(Boolean, default=False, nullable=False)
    dyspnoea = Column(Boolean, default=False, nullable=False)
    respiratory_symtoms = Column(Boolean, default=False, nullable=False)
    drowsiness = Column(Boolean, default=False, nullable=False)
    anosmia = Column(Boolean, default=False, nullable=False)
    dysguesia = Column(Boolean, default=False, nullable=False)
    observation = Column(String(150), nullable=True)
    evolution_id = Column(Integer, ForeignKey("evolutions.id", ondelete="CASCADE"))
    evolution = relationship(
        "Evolution",
        uselist=False,
        back_populates="evolution_template",
        cascade="all, delete",
    )
    medical_study = relationship(
        "MedicalStudy",
        uselist=False,
        back_populates="evolution_template_ms",
        cascade="all, delete",
    )
    treatment = relationship(
        "Treatment",
        uselist=False,
        back_populates="evolution_template_t",
        cascade="all, delete",
    )
    uti = relationship(
        "UTIInformation",
        uselist=False,
        back_populates="evolution_template_uti",
        cascade="all, delete",
    )

    def __repr__(self):
        return f"<EvolutionTemplate {self.id}>"


class Evolution(DateAware):
    __tablename__ = "evolutions"

    medic_id = Column(
        Integer, ForeignKey("medics.id", ondelete="CASCADE"), nullable=False
    )
    evolution_template = relationship(
        "EvolutionTemplate",
        uselist=False,
        back_populates="evolution",
        cascade="all, delete",
    )
    internement_id = Column(Integer, ForeignKey("internements.id", ondelete="CASCADE"))
    internement = relationship(
        "Internement", back_populates="evolution", cascade="all, delete"
    )
    system_id = Column(Integer)

    def __repr__(self):
        return f"<Evolution {self.id}>"


class Rule(DateAware):
    __tablename__ = "rules"

    drowsiness = Column(Boolean, nullable=False, default=True)
    ventilator_options = Column(String(150), nullable=True)
    breathing_frequency = Column(Integer, nullable=False)
    days_dif = Column(Integer, nullable=False)
    saturation = Column(Integer, nullable=False)
    saturation_compare = Column(Integer, nullable=False)

    def __repr__(self):
        return f"<Rule {self.id}>"

    def __init__(
        self,
        breathing_frequency,
        days_dif,
        saturation,
        saturation_compare,
        drowsiness=True,
    ):
        self.drowsiness = drowsiness
        self.breathing_frequency = breathing_frequency
        self.days_dif = days_dif
        self.saturation = saturation
        self.saturation_compare = saturation_compare
