from datetime import date
import datetime
from internement.serializers import InternementSystemSchema
from utils.errors import ConflictError
from utils.responses import (
    GenericCreateResponse,
    GenericListResponse,
    GenericRetrieveResponse,
    GenericDeleteResponse,
)
from evolution.serializers import (
    EvolutionSchema,
)
from evolution.models import (
    Evolution,
    EvolutionTemplate,
    MedicalStudy,
    Treatment,
    UTIInformation,
    Rule,
)
from sqlalchemy.sql import or_
from workforce.models import Medic, Patient
from internement.models import Internement, InternementSystem
from settings.database import Session
from http import HTTPStatus
from evolution.rules import Rules, RuleParam


# Create your responses here.


class EvolutionResponse(
    GenericCreateResponse, GenericRetrieveResponse, GenericListResponse
):
    model = Evolution
    schema = EvolutionSchema
    session = Session

    def list_evolution_change_internement(self, patient_id):
        internement = (
            Internement.query.filter_by(patient_id=patient_id)
            .order_by(Internement.id.desc())
            .first()
        )
        response = dict()
        if not internement:
            response["results"] = []
            return response, HTTPStatus.OK
        changes = InternementSystem.query.filter_by(internement_id=internement.id).all()

        if changes:
            # if len(changes) > 1:
            s_changes = InternementSystemSchema().dump(obj=changes, many=True)
            # else:
            # s_changes = InternementSystemSchema().dump(changes)
            for change in s_changes:
                evolutions = self.model.query.filter_by(
                    system_id=change["system_id"], internement_id=internement.id
                ).all()
                change["evolutions"] = self.schema().dump(obj=evolutions, many=True)

            response["results"] = s_changes

        return response, HTTPStatus.OK

    def lists(self, *args, **kwargs):
        page_size = kwargs["page_size"]
        page = kwargs["page"]

        page_from = (page - 1) * page_size

        patient = list()
        if "patient" in kwargs:
            patient.append(Patient.id == kwargs["patient"])

        evolutions = (
            self.session.query(Evolution)
            .join(Internement)
            .join(Patient)
            .filter(Evolution.created.between(kwargs["from"], kwargs["to"]))
            .filter(or_(*patient))
            .order_by(Evolution.id.asc())
            .limit(page_size)
            .offset(page_from)
        )

        total_results = Evolution.query.filter(
            Evolution.created.between(kwargs["from"], kwargs["to"])
        ).count()

        response = dict()
        list_deserialized = list()

        if evolutions:
            for obj in evolutions:
                # serializer = self._create_evolution_schema(obj)
                deserialized = self.schema().dump(obj)["evolution"]
                list_deserialized.append(deserialized)

        response["status"] = HTTPStatus.OK
        response["totalResults"] = total_results
        response["results"] = list_deserialized
        return response, HTTPStatus.OK

    def retrieve(self, id: int):
        evolution = Evolution.query.filter(Evolution.id == id).first()
        response = self.schema().dump(evolution)
        return response, HTTPStatus.OK

    def pre_create(self, *args, **kwargs):
        serialized = super().pre_create(*args, **kwargs)
        now = datetime.datetime.now()
        now_string = now.strftime("%Y-%m-%d %H:%M:%S")
        serialized["evolution_template"]["evolution_date"] = now_string
        return serialized

    def create_instance(self, *args, **kwargs):
        evolution_dict = dict()

        evolution = Evolution(medic_id=self.serializer["medic_id"])
        evolution_dict["evolution"] = evolution

        from flask_jwt_extended import get_jwt_identity

        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)

        evolution.system_id = system.id

        self.session.add(evolution)
        self.session.commit()

        medic = Medic.query.filter_by(id=self.serializer["medic_id"]).first()
        evolution_dict["medic"] = medic

        self.session.add(medic)
        self.session.commit()

        internement = Internement.query.filter_by(
            id=self.serializer["internement_id"]
        ).first()
        evolution.internement = internement
        evolution_dict["internement"] = internement

        evolution_template = EvolutionTemplate(**self.serializer["evolution_template"])
        evolution_template.evolution = evolution
        evolution_dict["evolution_template"] = evolution_template

        self.session.add(evolution_template)
        self.session.commit()

        if self.serializer.get("medical_study", None):
            medical_study = MedicalStudy(**self.serializer.get("medical_study"))
            medical_study.evolution_template_ms = evolution_template
            self.session.add(medical_study)
            self.session.commit()
            evolution_dict["medical_study"] = medical_study

        if self.serializer.get("treatment", None):
            treatment = Treatment(**self.serializer.get("treatment"))
            treatment.evolution_template_t = evolution_template
            self.session.add(treatment)
            self.session.commit()
            evolution_dict["treatment"] = treatment

        if self.serializer.get("uti", None):
            uti = UTIInformation(**self.serializer.get("uti"))
            uti.evolution_template_uti = evolution_template
            self.session.add(uti)
            self.session.commit()
            evolution_dict["uti"] = uti
        return evolution

    def post_create(self, instance):
        from datetime import datetime

        deserialized = super().post_create(instance)
        day = datetime.strptime(
            deserialized["evolution"]["evolution_template"]["evolution_date"],
            "%Y-%m-%d %H:%M:%S",
        ).day
        month = datetime.strptime(
            deserialized["evolution"]["evolution_template"]["evolution_date"],
            "%Y-%m-%d %H:%M:%S",
        ).month
        year = datetime.strptime(
            deserialized["evolution"]["evolution_template"]["evolution_date"],
            "%Y-%m-%d %H:%M:%S",
        ).year
        evolution_date = f"{day}/{month}/{year}"
        evolution_today = (
            f"{datetime.today().day}/{datetime.today().month}/{datetime.today().year}"
        )
        patient = (
            self.session.query(Patient)
            .join(Internement)
            .filter(Internement.id == deserialized["evolution"]["internement"]["id"])
            .first()
        )
        last_evolution = (
            self.session.query(Evolution)
            .join(Internement)
            .filter(Evolution.id != deserialized["evolution"]["id"])
            .filter(Internement.id == deserialized["evolution"]["internement"]["id"])
            .order_by(Evolution.id.desc())
            .limit(1)
            .all()
        )

        o2_saturation_last = deserialized["evolution"]["evolution_template"][
            "o2_saturation"
        ]
        if last_evolution:
            o2_saturation_last = last_evolution[0].evolution_template.o2_saturation

        param = RuleParam(
            deserialized["evolution"]["evolution_template"]["drowsiness"],
            deserialized["evolution"]["evolution_template"]["ventilator"],
            deserialized["evolution"]["evolution_template"]["breathing_frequency"],
            evolution_date,
            evolution_today,
            deserialized["evolution"]["evolution_template"]["o2_saturation"],
            o2_saturation_last,
        )
        rule = Rule.query.order_by(Rule.id.asc()).limit(1).first()
        Rules(
            drowsiness=rule.drowsiness,
            breathing_frequency=rule.breathing_frequency,
            days_dif=rule.days_dif,
            saturation=rule.saturation,
            saturation_compare=rule.saturation_compare,
        ).check_evolution(param, patient.id)
        return deserialized


class EvolutionPatientResponse(GenericRetrieveResponse):
    model = Evolution
    schema = EvolutionSchema
    session = Session

    def retrieve(self, id: int):
        instance = (
            Session.query(Evolution)
            .join(Internement)
            .join(Patient)
            .filter(Patient.id == id)
            .order_by(Evolution.id.desc())
            .first()
        )
        if not instance:
            raise ConflictError(
                user_err_msg=f"{self.model.__name__}({id}) does not exists."
            )
        response = self.schema().dump(instance)
        """if instance:
            response = EvolutionResponse._create_evolution_schema(
                instance, only_data=True
            )"""
        return response, HTTPStatus.OK
