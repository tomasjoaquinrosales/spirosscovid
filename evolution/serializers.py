from marshmallow.decorators import pre_dump
from system.models import System
from evolution.models import MedicalStudy, Treatment, UTIInformation, Evolution
from marshmallow import (
    Schema,
    fields,
    validate,
    validates_schema,
    ValidationError,
    validates,
    post_dump,
    pre_load,
)
from utils.serializers import BaseSchema
from utils.serializer_validations import must_not_be_blank
from workforce.serializers import MedicSchema
from workforce.models import Medic
from internement.serializers import InternementSchema


# Create your serializers here.


class EvolutionTemplateSchema(BaseSchema):
    evolution_date = fields.DateTime(format="%Y-%m-%d %H:%M:%S", allow_none=True)
    temperature = fields.Int(required=True, allow_none=False)
    blood_pressures_s = fields.Int(required=True, allow_none=False)
    blood_pressure_d = fields.Int(required=True, allow_none=False)
    cardiac_frequency = fields.Int(required=True, allow_none=False)
    breathing_frequency = fields.Int(required=True, allow_none=False)
    ventilator = fields.Str(
        required=True, allow_none=False, validate=validate.Length(min=4)
    )
    is_o2_required = fields.Boolean(default=False, allow_none=False)
    o2 = fields.Str(required=True, allow_none=True, validate=validate.Length(min=4))
    o2_saturation = fields.Int(required=True, allow_none=False)
    is_pafi_required = fields.Boolean(default=False, allow_none=False)
    pafi = fields.Int(required=True, allow_none=False)
    prono_vigil = fields.Boolean(default=False, allow_none=False)
    cough = fields.Boolean(default=False, allow_none=False)
    dyspnoea = fields.Boolean(default=False, allow_none=False)
    respiratory_symtoms = fields.Boolean(default=False, allow_none=False)
    drowsiness = fields.Boolean(default=False, allow_none=False)
    anosmia = fields.Boolean(default=False, allow_none=False)
    dysguesia = fields.Boolean(default=False, allow_none=False)
    observation = fields.Str(
        required=True, allow_none=True, validate=validate.Length(min=4)
    )

    @pre_load(pass_many=True)
    def remove_keys(self, data, many, **kwargs):
        if not many:
            if "id" in data:
                del data["id"]
            if "created" in data:
                del data["created"]
            if "modified" in data:
                del data["modified"]
            if "is_active" in data:
                del data["is_active"]
        return data


class MedicalStudySchema(BaseSchema):
    name = fields.Str(required=True, allow_none=False, validate=validate.Length(min=4))
    is_normal = fields.Boolean(required=True, allow_none=False)
    is_pathological = fields.Boolean(required=True, allow_none=False)
    description = fields.Str(
        required=True, allow_none=True, validate=validate.Length(min=4)
    )


class TreatmentSchema(BaseSchema):
    name = fields.Str(required=True, allow_none=True, validate=validate.Length(min=4))
    description = fields.Str(
        required=True, allow_none=True, validate=validate.Length(min=4)
    )


class UTISchema(BaseSchema):
    arm = fields.Boolean(required=True, allow_none=False)
    arm_description = fields.Str(
        required=True, allow_none=True, validate=validate.Length(min=4)
    )
    tracheostomy = fields.Boolean(required=True, allow_none=True)
    vasopressors = fields.Boolean(required=True, allow_none=True)
    vasopressors_description = fields.Str(
        required=True, allow_none=True, validate=validate.Length(min=4)
    )


class EvolutionSchema(BaseSchema):

    medic_id = fields.Int(required=True, allow_none=False, load_only=True)

    medic = fields.Nested(
        lambda: MedicSchema(exclude=["created", "modified"]),
        dump_only=True,
    )

    internement_id = fields.Int(required=True, allow_none=False, load_only=True)

    internement = fields.Nested(
        lambda: InternementSchema(exclude=["created", "modified"]),
        required=True,
        allow_none=False,
        dump_only=True,
    )

    system_id = fields.Int(required=False, allow_none=True)

    evolution_template = fields.Nested(
        EvolutionTemplateSchema,
        required=True,
        allow_none=False,
    )

    medical_study = fields.Nested(
        lambda: MedicalStudySchema(exclude=["created", "modified"]),
        required=True,
        allow_none=True,
    )

    treatment = fields.Nested(
        lambda: TreatmentSchema(exclude=["created", "modified"]),
        required=True,
        allow_none=True,
    )

    uti = fields.Nested(
        lambda: UTISchema(exclude=["created", "modified"]),
        required=True,
        allow_none=True,
    )

    @validates("medic_id")
    def validate_medic_id(self, value):
        medic = Medic.query.filter_by(id=value, is_active=True).first()
        if not medic:
            raise ValidationError("The medic_id does not exist or is inactive.")

    @validates("system_id")
    def validate_system_id(self, value):
        system = System.query.filter_by(id=value, is_active=True).first()
        if not system:
            raise ValidationError("The system_id does not exist or is inactive.")

    @post_dump(pass_many=True)
    def wrap_with_attributes(self, data, many, **kwargs):
        if many:
            for item in data:
                medic_id = Evolution.query.filter_by(id=item["id"]).first().medic_id
                medic = Medic.query.filter_by(id=medic_id).first()
                item["medic"] = MedicSchema().dump(medic)
                et_id = item["evolution_template"]["id"]
                ms = MedicalStudy.query.filter_by(evolution_template_id=et_id).first()
                item["medical_study"] = MedicalStudySchema().dump(ms)
                tr = Treatment.query.filter_by(evolution_template_id=et_id).first()
                item["treatment"] = TreatmentSchema().dump(tr)
                uti = UTIInformation.query.filter_by(
                    evolution_template_id=et_id
                ).first()
                item["uti"] = UTISchema().dump(uti)
            return data
        else:
            medic_id = Evolution.query.filter_by(id=data["id"]).first().medic_id
            medic = Medic.query.filter_by(id=medic_id).first()
            data["medic"] = MedicSchema().dump(medic)
            et_id = data["evolution_template"]["id"]
            ms = MedicalStudy.query.filter_by(evolution_template_id=et_id).first()
            data["medical_study"] = MedicalStudySchema().dump(ms)
            tr = Treatment.query.filter_by(evolution_template_id=et_id).first()
            data["treatment"] = TreatmentSchema().dump(tr)
            uti = UTIInformation.query.filter_by(evolution_template_id=et_id).first()
            data["uti"] = UTISchema().dump(uti)
            return {"evolution": data}
