from business_rule_engine import RuleParser
import formulas
from datetime import datetime

date_format = "%d/%m/%Y"

# EVOLUTION DATA

params = {
    'drowsiness': True,
    'ventilator': 'regular',
    'breathing_frequency': 50,
    'evolution_date': '10/11/2020',
    'today': '22/11/2020',
    'o2_saturation': 93,
    'o2_saturation_last': 96
}

# FUNCTIONS

FUNCTIONS = formulas.get_functions()
FUNCTIONS['FUNC_VENTILATOR'] = lambda x: x in ["regular", "bad"]
FUNCTIONS['FUNC_DAYS_DIFF'] = lambda x, y: (datetime.strptime(x, date_format) - datetime.strptime(y, date_format)).days
FUNCTIONS['FUNC_SATURATION_COMP'] = lambda saturation_t, saturation_y: 0 if saturation_t <= 92 else 100 - (saturation_t * 100 / saturation_y)


# CONTROL METHODS

def has_drowsiness(drowsiness):
    message = f'Somnolencia: evaular pase a UTI.'
    print(message)
    return message


def has_good_mechanic_ventilatory(ventilator):
    message = f'Mecánica Ventilatoria ({ventilator}): evaular pase a UTI.'
    print(message)
    return message


def has_normal_breathing_frequency(breathing_frequency):
    message = f'Frecuencia Respiratoria > ({breathing_frequency}): evaular pase a UTI.'
    print(message)
    return message


def has_passed_ten_days(evolution_date, today):
    quantity_day = (datetime.strptime(today, date_format) - datetime.strptime(evolution_date, date_format)).days
    message = f'Han pasado {quantity_day} días desde el inicio de los síntomas: evaular ALTA.'
    print(message)
    return message


def has_normal_o2_saturation(o2_saturation):
    message = f'Saturación de oxígeno < a {o2_saturation}. evaular oxigenoterapia y prono.'
    print(message)
    return message


def o2_saturation_compare(o2_saturation, o2_saturation_last):
    o2_saturation_percentage = o2_saturation - 100 / o2_saturation_last
    dif = 100 - o2_saturation_percentage
    message = f'Saturación de oxígeno bajo {dif}% respecto a la evolución anterior. evaular oxigenoterapia y prono.'
    print(message)
    return message


# RULES

rules = """
rule "drowsiness"
when
    drowsiness = True
then
    has_drowsiness(drowsiness)
end
rule "ventilator"
when
    FUNC_VENTILATOR(ventilator) = True
then
    has_good_mechanic_ventilatory(ventilator)
end
rule "breathing_frequency"
when
    breathing_frequency > 30
then
    has_normal_breathing_frequency(ventilator)
end
rule "evolution_date"
when
    FUNC_DAYS_DIFF(today, evolution_date) >= 10
then
    has_passed_ten_days(evolution_date, today)
end
rule "o2_saturation"
when
    o2_saturation <= 92
then
    has_normal_o2_saturation(o2_saturation)
end
rule "saturation compare"
when
    FUNC_SATURATION_COMP(o2_saturation, o2_saturation_last) >= 3
then
    o2_saturation_compare(o2_saturation, o2_saturation_last)
end
"""

# EXECUTION

if __name__ == '__main__':
    parser = RuleParser()
    parser.register_function(has_drowsiness)
    parser.register_function(has_good_mechanic_ventilatory)
    parser.register_function(has_normal_breathing_frequency)
    parser.register_function(has_passed_ten_days)
    parser.register_function(has_normal_o2_saturation)
    parser.register_function(o2_saturation_compare)
    parser.parsestr(rules)
    parser.execute(params, False)
    pass
