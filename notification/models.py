from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    ForeignKey,
    DateTime,
    Table,
    Boolean,
)
from sqlalchemy.orm import relationship
from settings.database import Base
from utils.models import DateAware
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required

# Create your model here.


class Notification(DateAware):
    __tablename__ = "notifications"

    medic_id = Column(Integer, ForeignKey("medics.id", ondelete="CASCADE"))
    medic = relationship("Medic", back_populates="notifications", cascade="all, delete")
    patient_id = Column(Integer, ForeignKey("patients.id", ondelete="CASCADE"))
    patient = relationship(
        "Patient", back_populates="notifications", cascade="all, delete"
    )
    message = Column(String())
    viewed = Column(Boolean, nullable=False, default=False)

    def __init__(self, medic_id, patient_id, message):
        self.medic_id = medic_id
        self.patient_id = patient_id
        self.message = message

    def __repr__(self):
        return f"<Notification (<Medic {self.medic_id}>, <Patient{self.patient_id}>) >"

    def normalize_message(self):
        json = dict()
        if self.message:
            for line in self.message.split('\n'):
                if line.strip():
                    key, message, action = line.lower().split(':')
                    json[key] = {
                        "message": message,
                        "acction": action
                    }
        return json
