from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required
from authentication.permissions import admin_required, chief_required, staff_required
from utils.decorators import ErrorHandler
import logging
from flask_cors import CORS
from notification.responses import NotificationResponse
from utils.requests import get_url_params


notification = Blueprint(
    "notification", __name__, template_folder="templates", static_folder="static"
)

CORS(notification)

logger = logging.getLogger("app")


# Create your end-points here.


@notification.route("", methods=["POST"])
@jwt_required
@ErrorHandler(logger, notification)
@staff_required
def add_notification():
    response, status_code = NotificationResponse(request).create()
    return response, status_code


@notification.route("", methods=["GET"])
@jwt_required
@ErrorHandler(logger, notification)
@staff_required
def notifications():
    filters = get_url_params(request)
    response, status_code = NotificationResponse(request).lists(**filters)
    return response, status_code


@notification.route("<int:notification_id>/viewed/", methods=["PUT"])
@jwt_required
@ErrorHandler(logger, notification)
@staff_required
def mark_as_viewed(notification_id):
    response, status_code = NotificationResponse(request).update(notification_id)
    return response, status_code


@notification.route("<int:notification_id>/unread/", methods=["PUT"])
@jwt_required
@ErrorHandler(logger, notification)
@staff_required
def mark_as_unread(notification_id):
    response, status_code = NotificationResponse(request).update(notification_id, False)
    return response, status_code