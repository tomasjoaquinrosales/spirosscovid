from marshmallow import Schema, fields, validate, validates, ValidationError
from utils.serializers import BaseSchema
from utils.serializer_validations import must_not_be_blank
from workforce.serializers import PatientSchema, MedicSchema
from workforce.models import Medic, Patient


# Create your serializers here.


class NotificationSchema(BaseSchema):

    medic_id = fields.Int(required=True, allow_none=False)
    medic = fields.Nested(
        lambda: MedicSchema(exclude=["created", "modified", "user"]), dump_only=True
    )
    patient_id = fields.Int(required=True, allow_none=False)
    patient = fields.Nested(
        lambda: PatientSchema(exclude=["created", "modified"]), dump_only=True
    )
    viewed = fields.Boolean(dump_only=True)
    message = fields.Str(required=True, allow_none=False)

    @validates("medic_id")
    def validate_medic_id(self, value):
        medic = Medic.query.filter_by(id=value).first()
        if not medic:
            raise ValidationError(f"Medic does not exists.")

    @validates("patient_id")
    def validate_patient_id(self, value):
        patient = Patient.query.filter_by(id=value).first()
        if not patient:
            raise ValidationError(f"Patient does not exist")
