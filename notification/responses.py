from sqlalchemy import and_
from sqlalchemy.exc import DatabaseError, IntegrityError
from marshmallow import ValidationError
from utils.errors import ConflictError, NotFoundError, ClientException
from utils.responses import (
    GenericCreateResponse,
    GenericListResponse,
    GenericRetrieveResponse,
    GenericDeleteResponse,
)
from flask_jwt_extended import get_jwt_identity
from settings.database import Session
from http import HTTPStatus
from notification.models import Notification
from notification.serializers import NotificationSchema
from app import pusher_client
from os import path, environ
from internement.models import Internement, internements_medics
from workforce.models import Medic, User, Patient
from flask import jsonify

# Create your responses here.


PUSHER_EVENT = "messages"


class NotificationResponse(GenericListResponse, GenericCreateResponse):
    model = Notification
    schema = NotificationSchema
    session = Session

    @staticmethod
    def get_pusher_room(patient_id):
        response = list()
        medics = list()
        internement = (
            NotificationResponse.session.query(Internement)
            .join(Patient)
            .filter(and_(Internement.check_out.is_(None), Internement.date_of_death.is_(None)))
            .filter(Patient.id == patient_id)
            .order_by(Internement.id.desc())
            .first()
        )

        if internement:
            response.append(internement.id)
            for m in internement.medic:
                medics.append(m.user.username)
        return response, medics

    @staticmethod
    def get_patient_internement(patient_id):

        internement = (
            NotificationResponse.session.query(Internement)
            .join(Patient)
            .filter(and_(Internement.check_out.is_(None), Internement.date_of_death.is_(None)))
            .filter(Patient.id == patient_id)
            .order_by(Internement.id.desc())
            .first()
        )

        if not internement:
            return 0, HTTPStatus.OK

        return internement.id

    @staticmethod
    def send_pusher_notification(body, patient_id):
        message = dict()
        message["notification"] = body
        pusher_rooms, medics = NotificationResponse.get_pusher_room(patient_id)
        message["active_room"] = rooms = dict()
        username = get_jwt_identity()["username"]
        _, system = Medic.get_by_username(username)
        rooms['room'] = system.name.lower()
        rooms['active'] = False
        if pusher_rooms and medics:
            rooms['room'] = system.name.lower()
            rooms['active'] = True
            rooms['medics'] = medics
        pusher_client.trigger(environ.get("PUSHER_CHANNEL"), PUSHER_EVENT, message)

    def lists(self, *args, **kwargs):

        page_size = kwargs["page_size"]
        page = kwargs["page"]

        page_from = (page - 1) * page_size

        username = get_jwt_identity()["username"]

        notifications = (
            self.session.query(Notification)
            .join(Medic)
            .join(User)
            .filter(User.username == username)
            .order_by(Notification.id.desc())
            .limit(page_size)
            .offset(page_from)
            .all()
        )

        total_results = (
            self.session.query(Notification)
            .join(Medic)
            .join(User)
            .filter(User.username == username)
            .count()
        )

        unread = (
            self.session.query(Notification)
            .join(Medic)
            .join(User)
            .filter(User.username == username)
            .filter(Notification.viewed.is_(False))
            .count()
        )

        response = dict()
        list_serializer = list()

        if notifications:
            for obj in notifications:
                serializer = self.schema().dump(obj)
                list_serializer.append(serializer)

        response["status"] = HTTPStatus.OK
        response["totalResults"] = total_results
        response["results"] = list_serializer
        response["unread"] = unread
        return response, HTTPStatus.OK

    def update(self, notification_id, viewed=True):
        notification = Notification.query.filter_by(id=notification_id).first()
        if not notification:
            raise ConflictError(
                user_err_msg=f"{self.model.__name__}({id}) does not exists."
            )
        notification.viewed = viewed
        self.session.add(notification)
        self.session.commit()
        return self.schema().dump(notification), HTTPStatus.OK
